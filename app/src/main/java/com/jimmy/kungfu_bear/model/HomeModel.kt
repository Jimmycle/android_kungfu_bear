package com.jimmy.kungfu_bear.model

/**
 * "title": "重磅推荐",
"title_en": "HEAVY RECOMMENDATION",
"list": [
{
"title": "测试图片",
"title_en": "test_img",
"type": "image",
"content": "[\"products/202001/15/1_1579087739_8d5fC0gVCF.png\",\"products/202001/15/1_1579069583_eDB71AzltI.jpeg\",\"products/202001/15/1_1579070009_oRXNLEXuYB.jpeg\"]",
"image": [
"http://markdowni.oss-cn-shanghai.aliyuncs.com/products/202001/15/1_1579087739_8d5fC0gVCF.png?OSSAccessKeyId=LTAI9HPOjkj6nowY&Expires=1579347993&Signature=WjHbXywE7OYr0QT%2Fu4qP0O3dSNw%3D",
"http://markdowni.oss-cn-shanghai.aliyuncs.com/products/202001/15/1_1579069583_eDB71AzltI.jpeg?OSSAccessKeyId=LTAI9HPOjkj6nowY&Expires=1579347993&Signature=H8VOKERi%2F1AxWnMZdW15CR0WfXw%3D",
"http://markdowni.oss-cn-shanghai.aliyuncs.com/products/202001/15/1_1579070009_oRXNLEXuYB.jpeg?OSSAccessKeyId=LTAI9HPOjkj6nowY&Expires=1579347993&Signature=DnJhD4ymlG2aImyXSgqkw5d9UNc%3D"
]
},
{
"title": "测试视频",
"title_en": "test_video",
"type": "video",
"content": "products/202001/15/1_1579087739_8d5fC0gVCF.png",
"video": "http://markdowni.oss-cn-shanghai.aliyuncs.com/products/202001/15/1_1579087739_8d5fC0gVCF.png?OSSAccessKeyId=LTAI9HPOjkj6nowY&Expires=1579347993&Signature=WjHbXywE7OYr0QT%2Fu4qP0O3dSNw%3D"
}
]
 */
data class HomeModel(
    val title: String,
    val title_en: String,
    val list: List<HomeItem>
)

data class HomeItem(
    val title: String,
    val title_en: String,
    val type: String,
    val content: String,
    val image: ArrayList<String>,
    val video: String
)