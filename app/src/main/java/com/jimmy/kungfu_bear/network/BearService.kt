package com.jimmy.kungfu_bear.network

import com.jimmy.kungfu_bear.model.HomeModel
import com.jimmy.kungfu_bear.model.PlaceModel
import com.jimmy.kungfu_bear.model.SignInModel
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface BearService {

    /**
     * 获取场地列表
     */
    @GET("/api/app_venue")
    fun getPlaceList(): Observable<BearResponse<List<PlaceModel>>>

    /**
     * 获取首页展示信息
     */
    @GET("/api/get_ad")
    fun getHomeData(): Observable<BearResponse<HomeModel>>


    /**
     * 获取用户签到信息
     * code 二维码信息
     * venue_id 场所Id
     */
    @GET("/api/app_today_lesson")
    fun getSignInfo(
        @Query("code") code: String,
        @Query("venue_id") venue_id: Int
    ): Observable<BearResponse<SignInModel>>


    /**
     * 签到
     */
    @POST("/api/app_sign_in")
    fun sign(@Body requestBody: RequestBody):Observable<BearResponse<Any>>

}