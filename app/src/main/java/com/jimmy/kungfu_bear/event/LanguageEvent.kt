package com.jimmy.kungfu_bear.event

/**
 * 语言切换时间
 */
class LanguageEvent constructor(val msg: String)