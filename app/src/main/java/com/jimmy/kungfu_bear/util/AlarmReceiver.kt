package com.jimmy.kungfu_bear.util

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.model.HomeModel
import com.jimmy.kungfu_bear.network.BearApi
import com.jimmy.kungfu_bear.network.BearResponse
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val calendar = Calendar.getInstance()
        @SuppressLint("SimpleDateFormat") val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = sdf.format(calendar.time)
        Log.d("Timer", "定时任务开始 date > $date")
        BearApi.getService(context).getHomeData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<BearResponse<HomeModel>> {
            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(it: BearResponse<HomeModel>) {
                if (it.hasData) {
                    val jsonHomeCacheData = SpUtil.open(context).getSpData(ProjectConst.SP_HOME_DATA, "")
                    if (Gson().toJson(it.data) != jsonHomeCacheData) {
                        SpUtil.open(context).putSpData(ProjectConst.SP_HOME_DATA, Gson().toJson(it.data))
                    }
                }
                val newIntent = Intent(BroadcastConst.ACTION_STATUS)
                newIntent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_HOME_UPDATE_TASK)
                LocalBroadcastManager.getInstance(context).sendBroadcastSync(newIntent)
            }

            override fun onComplete() {
            }

            override fun onError(e: Throwable) {
            }
        })
    }
}