package com.jimmy.kungfu_bear

import android.content.Context
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.danikula.videocache.HttpProxyCacheServer
import com.jimmy.kungfu_bear.util.HttpLUtil


class App : MultiDexApplication() {

    private val proxy: HttpProxyCacheServer? = null

    companion object {
        lateinit var instance: App
            private set

        fun getProxy(context: Context): HttpProxyCacheServer {
            return if (instance.proxy == null) {
                instance.newProxy()
            } else {
                instance.proxy!!
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initialization()
    }

    private fun initialization() {
        HttpLUtil.logEnable = BuildConfig.DEBUG
        Log.d("App", "initialization")
    }

    private fun newProxy(): HttpProxyCacheServer { // 1 Gb for cache
        return HttpProxyCacheServer.Builder(this).maxCacheSize(1024 * 1024 * 1024).maxCacheFilesCount(20).build()
    }
}