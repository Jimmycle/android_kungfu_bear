package com.jimmy.kungfu_bear.callback

interface OnHomeVideoItemFinished {
    fun onVideoFinished(position: Int)
    fun recordViewProgress(progress: Int)
}

