package com.jimmy.kungfu_bear.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.ResponseBody
import org.json.JSONObject

/**
 * 公共头拦截器
 */
object HeaderInterceptor {

    /**
     * 处理token过期重新登录
     */
    fun init(context: Context): Interceptor {
        return init(context, object : OnHttpErrorListener {
            override fun onError(code: String, errMsg: String) {
                when (code) {
                    "500" -> {  // 特殊code处理

                    }
                }
            }
        })
    }

    private fun init(context: Context, onHttpErrorListener: OnHttpErrorListener) =
        Interceptor { chain ->
            val newBuilder = chain.request().newBuilder()
                .addHeader("X-Requested-With", "XMLHttpRequest")
                .addHeader("Accept", "application/json")
            val request = newBuilder.build()
            val response = chain.proceed(request)
            val mediaType = response.body()?.contentType()
            val content = response.body()?.string()
            try {
                val code = JSONObject(content).getString("code")
                val status = JSONObject(content).getString("status")
                if (!code.startsWith("2")) {
                    onHttpErrorListener.onError(code, status)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (content != null) {
                response.newBuilder().body(ResponseBody.create(mediaType, content)).build()
            } else {
                response.newBuilder().build()
            }
        }

    interface OnHttpErrorListener {
        fun onError(code: String, errMsg: String)
    }

}