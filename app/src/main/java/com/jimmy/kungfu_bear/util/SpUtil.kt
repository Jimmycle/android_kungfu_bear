package com.jimmy.kungfu_bear.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

/**
 * SharedPreferences 存储工具
 */
class SpUtil private constructor(ctx: Context) {
    private val pref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)

    companion object {
        private var sPrefs: SpUtil? = null

        fun open(ctx: Context): SpUtil {
            if (null == sPrefs) {
                sPrefs = SpUtil(ctx)
            }
            return sPrefs!!
        }

        fun <T> stringToArray(s: String, clazz: Class<Array<T>>): List<T> {
            val arr = Gson().fromJson(s, clazz)
            return if (null != arr && arr.isNotEmpty()) {
                Arrays.asList(*arr)
            } else {
                emptyList()
            }
        }

    }

    fun <T> putSpData(name: String, value: T) = with(pref.edit()) {
        when (value) {
            is Long -> putLong(name, value)
            is String -> putString(name, value)
            is Int -> putInt(name, value)
            is Boolean -> putBoolean(name, value)
            is Float -> putFloat(name, value)
            else -> {
                putString(name, Gson().toJson(value))
            }
        }.apply()
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNCHECKED_CAST")
    fun <T> getSpData(name: String, default: T): T = with(pref) {
        val res: Any
        when (default) {
            is Int -> res = getInt(name, default)
            is Long -> res = getLong(name, default)
            is String -> res = getString(name, default)
            is Boolean -> res = getBoolean(name, default)
            is Float -> res = getFloat(name, default)
            else -> {
                res = getObject(getString(name, Gson().toJson(default)), object : TypeToken<T>() {}.rawType)!!
            }
        }
        return res as T
    }

    private fun <T> getObject(obj: String, cls: Class<T>): T {
        return Gson().fromJson<T>(obj, cls)
    }

    fun remove(key: String) {
        pref.edit().remove(key).apply()
    }

}