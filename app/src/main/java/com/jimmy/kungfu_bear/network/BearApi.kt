package com.jimmy.kungfu_bear.network

import android.annotation.SuppressLint
import android.content.Context
import com.jimmy.kungfu_bear.config.ServerUrl
import com.jimmy.kungfu_bear.network.log.LogInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class BearApi private constructor(context: Context) {
    private var retrofit: Retrofit

    init {
        val client = OkHttpClient.Builder()
            .addInterceptor(HeaderInterceptor.init(context))
            .addInterceptor(LogInterceptor.init())
            .retryOnConnectionFailure(true)
            .readTimeout(ServerUrl.READ_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(ServerUrl.CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(ServerUrl.HOST_CURRENT)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var sApi: BearApi? = null

        @Synchronized
        private fun getApi(context: Context): BearApi {
            if (null == sApi) {
                sApi = BearApi(context)
            }
            return sApi!!
        }

        private fun <T> getService(context: Context, service: Class<T>): T {
            return getApi(context).retrofit.create(service)
        }

        fun getService(context: Context): BearService {
            return getService(context, BearService::class.java)
        }
    }

}