package com.jimmy.kungfu_bear.config

object ProjectConst {

    const val TYPE_IMAGE = "image"
    const val TYPE_VIDEO = "video"

    const val STATUS_SIGN_IN = "status_sign_in"
    const val STATUS_SIGN_SUCCESS = "status_sign_success"

    const val SP_LANGUAGE = "sp_language"
    const val LANGUAGE_EN = "language_en"
    const val LANGUAGE_CN = "language_cn"

    const val SP_HOME_DATA = "sp_home_data"

    const val CN_TITLE = "熊小武功夫馆"
    const val CN_LOADING_VIDEO_ERROR = "加载视频源错误"
    const val CN_EXIT = "退出"
    const val CN_SYSTEM_ERROR = "系统异常，请联系工作人员"
    const val CN_REMAINS_TIME = "操作还剩:"
    const val CN_SIGN_IN = "签到"
    const val CN_SIGN_IN_ALL = "全部已签到"
    const val CN_WELCOME = "欢迎您，"
    const val CN_TODAY = "今日"
    const val CN_FOR_YOU = "您为"
    const val CN_FOR_YOU_LESSON = "预约了以下课程"
    const val CN_SIGN_IN_LESSON = "，请选择对应课程完成签到"
    const val CN_SIGN_IN_LESSON_HINT = "课中或者课后也可签到"
    const val CN_MEMBER_ID = "会员号"
    const val CN_FOR_YOU_LESSON_SUCCESS = "本次您为"
    const val CN_SIGN_IN_LESSON_SUCCESS = "选择的以下课程签到成功"
    const val CN_SIGN_IN_FAILED = "签到失败"
    const val CN_SIGN_IN_LIMIT = "请至少选择一门课程"
    const val CN_LOADING ="正在加载中..."


    const val EN_TITLE = "Kungfu Bear"
    const val EN_LOADING_VIDEO_ERROR = "Error loading video source"
    const val EN_EXIT = "Exit"
    const val EN_SYSTEM_ERROR = "System is abnormal, please contact the staff"
    const val EN_REMAINS_TIME = "remain times:"
    const val EN_SIGN_IN = "Sign In"
    const val EN_SIGN_IN_ALL = " All Sign In"
    const val EN_WELCOME = "Welcome，"
    const val EN_TODAY = "Today is "
    const val EN_FOR_YOU = "You have reserved the courses for"
    const val EN_FOR_YOU_LESSON = ""
    const val EN_SIGN_IN_LESSON = "，Please select the course to complete sign-in"
    const val EN_SIGN_IN_LESSON_HINT = "You can also sign in during class or after class"
    const val EN_MEMBER_ID = "Member ID"
    const val EN_FOR_YOU_LESSON_SUCCESS = "The courses you selected for"
    const val EN_SIGN_IN_LESSON_SUCCESS = "have successfully signed in"
    const val EN_SIGN_IN_FAILED = "sign in failed"
    const val EN_SIGN_IN_LIMIT = "Please select at least one course"
    const val EN_LOADING ="Loading..."



}