package com.jimmy.kungfu_bear.util

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

object JsonUtil {

    fun jsonFormat(jsonData: String): String {
        try {
            val jsonText = jsonData.trim { it <= ' ' }
            if (jsonText.startsWith("{"))
                return JSONObject(jsonText).toString(2)
            if (jsonText.startsWith("[")) {
                return JSONArray(jsonText).toString(2)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonData
    }

    fun isJson(text: String?): Boolean {
        if (!text.isNullOrEmpty()) {
            val jsonText = text.trim { it <= ' ' }
            return (jsonText.startsWith("{") && jsonText.endsWith("}")) || (jsonText.startsWith("[") && jsonText.endsWith("]"))
        }
        return false
    }

}