package com.jimmy.kungfu_bear.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.callback.OnHomeImageItemFinished
import com.jimmy.kungfu_bear.callback.OnHomeListItemSelected
import com.jimmy.kungfu_bear.callback.OnHomeVideoItemFinished
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.contract.HomeContract
import com.jimmy.kungfu_bear.model.HomeModel
import com.jimmy.kungfu_bear.presenter.HomePresenter
import com.jimmy.kungfu_bear.util.replaceChildFragment
import com.trello.rxlifecycle3.components.support.RxFragment

/**
 * 首页
 */
class HomeFragment : BaseAppFragment<HomePresenter, HomeContract.View>(), HomeContract.View, OnHomeListItemSelected, OnHomeImageItemFinished, OnHomeVideoItemFinished {

    private var homeListFragment = HomeListFragment()
    private var currentLeftFragment: RxFragment? = null
    private var homeData: HomeModel? = null
    private var dataSize = 0
    private var currentPosition = 0
    private var currentProgress = 0

    override fun getLayoutID(): Int = R.layout.fragment_home

    override fun initPresenter(): HomePresenter = HomePresenter(this, this)

    companion object {
        fun newInstance(position: Int, progress: Int): HomeFragment {
            val fragment = HomeFragment()
            val bundle = Bundle()
            bundle.putInt("position", position)
            bundle.putInt("progress", progress)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        currentPosition = arguments?.getInt("position", 0)!!
        currentProgress = arguments?.getInt("progress", 0)!!
        mPresenter?.getHomeData()
    }

    override fun initHomeView(data: HomeModel?) {
        homeData = data
        if (homeData != null && !homeData?.list.isNullOrEmpty()) {
            dataSize = homeData?.list!!.size
            homeListFragment = HomeListFragment.newInstance(homeData!!, currentPosition)
            this.replaceChildFragment(homeListFragment, R.id.fl_container_right)
        }
    }

    override fun onListSelect(position: Int, isAuto: Boolean) {
        Log.d("Home", "HomeFragment onListSelect")
        val homeItem = homeData?.list?.get(position)
        currentPosition = position
        if (!isAuto) {
            currentProgress = 0
        }
        if (this.isAdded) {
            when (homeItem?.type) {
                ProjectConst.TYPE_IMAGE -> {
                    currentLeftFragment = HomeImageFragment.newInstance(homeItem.image, currentPosition)
                    this.replaceChildFragment(currentLeftFragment as HomeImageFragment, R.id.fl_container_left)
                }
                ProjectConst.TYPE_VIDEO -> {
                    currentLeftFragment = HomeVideoFragment.newInstance(homeItem.video, currentPosition, currentProgress)
                    this.replaceChildFragment(currentLeftFragment as HomeVideoFragment, R.id.fl_container_left)
                }
            }
        }
    }

    override fun onImageFinished(position: Int) {
        updateListFragment(position)
        Log.d("Home", "HomeFragment onImageFinished")
    }

    override fun onVideoFinished(position: Int) {
        updateListFragment(position)
        Log.d("Home", "HomeFragment onVideoFinished")
    }

    override fun recordViewProgress(progress: Int) {
        currentProgress = progress
    }

    private fun updateListFragment(position: Int) {
        if (dataSize != 0) {
            currentPosition = position % dataSize
        }
        currentProgress = 0
        val intent = Intent(BroadcastConst.ACTION_STATUS)
        intent.putExtra(BroadcastConst.KEY_HOME_VIDEO_PROGRESS, currentProgress)
        LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
        if (this.isAdded && homeListFragment.isAdded) {
            homeListFragment.refreshList(currentPosition)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("Home", "HomeFragment onDestroyView")
    }

}