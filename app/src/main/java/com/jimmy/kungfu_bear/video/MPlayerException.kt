package com.jimmy.kungfu_bear.video

class MPlayerException(message: String?, cause: Throwable?) : Exception(message, cause)