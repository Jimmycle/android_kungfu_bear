package com.jimmy.kungfu_bear.contract

import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.model.SignInModel

interface MainContract {
    interface View : BaseContract.IView {
        fun signInfoSuccess(data:SignInModel)
        fun signInfoFailed()
    }

    interface Presenter : BaseContract.IPresenter<View> {
        fun getSignInfo(code: String, venue_id: Int)
        fun getHomeData()
    }

}