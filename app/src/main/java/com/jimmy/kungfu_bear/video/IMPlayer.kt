package com.jimmy.kungfu_bear.video

import android.content.res.AssetFileDescriptor

interface IMPlayer {

    /**
     * 设置资源
     * @param url 资源路径url
     * @param position 开始播放位置
     */
    @Throws(MPlayerException::class)
    fun setSource(url: String, position: Int)

    /**
     * 设置资源
     * @param assetFd 本地资源路径AssetFileDescriptor
     * @param position 开始播放位置
     */
    fun setSourceAssetFd(assetFd: AssetFileDescriptor, position: Int)

    /**
     * 播放视频
     */
    @Throws(MPlayerException::class)
    fun play()

    fun pause()

    fun resume()

    fun onResume()

    fun onPause()

    fun onDestroy()
}