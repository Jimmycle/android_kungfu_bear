package com.jimmy.kungfu_bear.fragment

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.contract.PlaceContract
import com.jimmy.kungfu_bear.model.PlaceModel
import com.jimmy.kungfu_bear.presenter.PlacePresenter
import kotlinx.android.synthetic.main.fragment_place.*

/**
 * 场地选择
 */
class PlaceFragment : BaseAppFragment<PlacePresenter, PlaceContract.View>(), PlaceContract.View {

    private var adapter: ArrayAdapter<CharSequence>? = null
    private var currentPlaceName = ""
    private var currentPlaceNameEN = ""
    private var currentPlaceId = -1

    override fun getLayoutID(): Int = R.layout.fragment_place

    override fun initPresenter(): PlacePresenter = PlacePresenter(this, this)

    override fun initView() {
        mPresenter?.getPlaceList()
        btn_sure.setOnClickListener {
            if (currentPlaceName.isEmpty()) {
                toastFail(resources.getString(R.string.place_title))
            } else {
                val intent = Intent(BroadcastConst.ACTION_STATUS)
                intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_HOME)
                intent.putExtra(BroadcastConst.KEY_PLACE_NAME, currentPlaceName)
                intent.putExtra(BroadcastConst.KEY_PLACE_NAME_EN, currentPlaceNameEN)
                intent.putExtra(BroadcastConst.KEY_PLACE_ID, currentPlaceId)
                LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
            }
        }
    }

    override fun updateSpinner(data: List<PlaceModel>) {
        var isFirst = true
        spinner.visibility = View.VISIBLE
        tv_default.visibility = View.VISIBLE

        val arrName = Array(data.size) { "" }
        val arrNameEn = Array(data.size) { "" }
        val arrId = Array(data.size) { -1 }
        for (idx in data.indices) {
            arrName[idx] = data[idx].title
            arrNameEn[idx] = data[idx].title_en
            arrId[idx] = data[idx].id
        }
        adapter = ArrayAdapter(activity!!, R.layout.layout_spinner, arrName)
        adapter?.setDropDownViewResource(R.layout.item_spinner)
        spinner.adapter = adapter
        spinner.setSelection(-1)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d("123", "position$position")
                if (isFirst) {
                    currentPlaceName = ""
                    currentPlaceNameEN = ""
                    currentPlaceId = -1
                } else {
                    tv_default.visibility = View.GONE
                    currentPlaceName = arrName[position]
                    currentPlaceNameEN = arrNameEn[position]
                    currentPlaceId = arrId[position]
                }
                isFirst = false
            }
        }
    }

    override fun getPlaceFailed() {
        spinner.visibility = View.GONE
        tv_default.visibility = View.VISIBLE
    }


}