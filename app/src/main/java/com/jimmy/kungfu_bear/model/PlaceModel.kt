package com.jimmy.kungfu_bear.model

/**
 *   {
"id": 1,
"title": "小武馆",
"title_en": "abc",
"full_address": "",
"business_hour": " - ",
"format_logo": "object name is empty",
"format_image": "object name is empty",
"format_service_qrcode": "object name is empty"
},
 */
data class PlaceModel(
    val id: Int,
    val title: String,
    val title_en: String,
    val full_address: String,
    val business_hour: String,
    val format_logo: String,
    val format_image: String,
    val format_service_qrcode: String
)