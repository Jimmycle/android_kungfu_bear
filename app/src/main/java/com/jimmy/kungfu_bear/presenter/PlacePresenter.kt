package com.jimmy.kungfu_bear.presenter

import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.contract.PlaceContract
import com.jimmy.kungfu_bear.network.BearApi
import com.trello.rxlifecycle3.components.support.RxFragment

class PlacePresenter(fragment: RxFragment, view: PlaceContract.View) : BasePresenter<PlaceContract.View>(fragment, view), PlaceContract.Presenter {

    override fun getPlaceList() {
        BearApi.getService(mFragment?.activity!!).getPlaceList().execute {
            onNext {
                if (it.hasData) {
                    mView?.updateSpinner(it.data!!)
                } else {
                    mView?.getPlaceFailed()
                }
            }

            onError {
                mView?.getPlaceFailed()
            }
        }
    }

}