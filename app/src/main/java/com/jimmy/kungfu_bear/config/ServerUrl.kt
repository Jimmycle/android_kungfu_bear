package com.jimmy.kungfu_bear.config

object ServerUrl {
    /**
     * 域名url
     */
    const val HOST_ONLINE = "https://test.xiongxiaowu.com"

    /**
     * 当前默认url
     */
    var HOST_CURRENT = HOST_ONLINE

    /**
     * 超时时间(s)
     */
    const val READ_TIMEOUT = 10L
    const val CONNECT_TIMEOUT = 10L

}