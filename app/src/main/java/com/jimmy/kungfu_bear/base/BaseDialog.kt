package com.jimmy.kungfu_bear.base

import android.app.Activity
import android.app.Dialog
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.jimmy.kungfu_bear.R

class BaseDialog private constructor() {

    companion object {
        val instance: BaseDialog by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            BaseDialog()
        }
    }

    /**
     * 显示加载对话框
     */
    fun showLoadingProgressDialog(activity: Activity, content: String): Dialog {
        val dialog = Dialog(activity, R.style.DialogFromCenter)
        dialog.setContentView(R.layout.dialog_loading_progress)
        val lay = dialog.window!!.attributes
        lay.width = WindowManager.LayoutParams.WRAP_CONTENT
        lay.height = WindowManager.LayoutParams.WRAP_CONTENT
        lay.gravity = Gravity.CENTER
        dialog.window!!.attributes = lay
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.show()
        fullScreenImmersive(dialog.window?.decorView)
        dialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.findViewById<TextView>(R.id.tv_tip).text = content
        return dialog
    }

    /**
     * Toast
     * 文字
     */
    fun showToastTextDialog(activity: Activity, content: String) {
        val dialog = Dialog(activity, R.style.DialogNoDim)
        dialog.setContentView(R.layout.dialog_toast_text)
        val lay = dialog.window!!.attributes
        lay.width = WindowManager.LayoutParams.WRAP_CONTENT
        lay.height = WindowManager.LayoutParams.WRAP_CONTENT
        lay.gravity = Gravity.CENTER
        dialog.window!!.attributes = lay
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.show()
        fullScreenImmersive(dialog.window?.decorView)
        dialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        val tvContent = dialog.findViewById<TextView>(R.id.tv_content)
        tvContent.text = content
        Handler().postDelayed({
            if (dialog.isShowing && !activity.isFinishing) {
                dialog.dismiss()
            }
        }, 2000)
    }

    fun showToastSuccess(activity: Activity, content: String): Dialog {
        return showToastCustomDialog(activity, content, R.mipmap.ic_toast_success)
    }

    fun showToastFail(activity: Activity, content: String): Dialog {
        return showToastCustomDialog(activity, content, R.mipmap.ic_toast_fail)
    }

    fun showToastError(activity: Activity, content: String): Dialog {
        return showToastCustomDialog(activity, content, R.mipmap.ic_toast_error)
    }

    /**
     * 显示Toast
     * 图片+文字
     */
    private fun showToastCustomDialog(activity: Activity, content: String, resId: Int): Dialog {
        val dialog = Dialog(activity, R.style.DialogNoDim)
        dialog.setContentView(R.layout.dialog_toast_custom)
        val lay = dialog.window!!.attributes
        lay.width = WindowManager.LayoutParams.WRAP_CONTENT
        lay.height = WindowManager.LayoutParams.WRAP_CONTENT
        lay.gravity = Gravity.CENTER
        dialog.window!!.attributes = lay
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.show()
        fullScreenImmersive(dialog.window?.decorView)
        dialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.findViewById<ImageView>(R.id.toast_icon).setImageResource(resId)
        dialog.findViewById<TextView>(R.id.toast_content).text = content
        Handler().postDelayed({
            if (dialog.isShowing && !activity.isFinishing) {
                dialog.dismiss()
            }
        }, 1600)
        return dialog
    }

    private fun fullScreenImmersive(view: View?) {
        val uiOptions =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_FULLSCREEN
        view?.systemUiVisibility = uiOptions
    }

}