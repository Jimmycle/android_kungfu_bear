package com.jimmy.kungfu_bear.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Message
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.google.gson.Gson
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.base.BaseHandler
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.config.ProjectGlobal
import com.jimmy.kungfu_bear.contract.SignInContract
import com.jimmy.kungfu_bear.event.LanguageEvent
import com.jimmy.kungfu_bear.model.SignInItem
import com.jimmy.kungfu_bear.model.SignInModel
import com.jimmy.kungfu_bear.presenter.SignInPresenter
import kotlinx.android.synthetic.main.fragment_signin.*
import kotlinx.android.synthetic.main.layout_count_down.view.*
import kotlinx.android.synthetic.main.layout_signin_info.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * 签到
 */
class SignInFragment : BaseAppFragment<SignInPresenter, SignInContract.View>(), SignInContract.View {

    private var currentStatus = ProjectConst.STATUS_SIGN_IN
    private var baseHandler: BaseHandler? = null
    private var msgCountDown = 0x66
    private var times = 60
    private var adapter: SignInAdapter? = null
    private var checkCollections = hashMapOf<Int, Boolean>()
    private val selectSignIds = arrayListOf<Int>()
    private var signData: SignInModel? = null
    private var qRCode = ""
    private var venueId = -1
    private var isAllSignIn = false

    override fun getLayoutID(): Int = R.layout.fragment_signin

    override fun initPresenter(): SignInPresenter = SignInPresenter(this, this)

    companion object {
        fun newInstance(data: SignInModel, qRCode: String, venueId: Int): SignInFragment {
            val fragment = SignInFragment()
            val bundle = Bundle()
            val jsonData = Gson().toJson(data)
            bundle.putString("data", jsonData)
            bundle.putString("qRCode", qRCode)
            bundle.putInt("venueId", venueId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        val jsonData = arguments?.getString("data")
        qRCode = arguments?.getString("qRCode")!!
        venueId = arguments?.getInt("venueId")!!
        signData = Gson().fromJson(jsonData, SignInModel::class.java)

        initLayout()
        init()
        initListener()
        onLanguageChange(LanguageEvent(ProjectGlobal.currentLanguage))
        EventBus.getDefault().register(this)
    }

    private fun initLayout() {
        when (currentStatus) {
            ProjectConst.STATUS_SIGN_IN -> {
                layout_signin_info.visibility = View.VISIBLE
                layout_signin_success.visibility = View.GONE
                btn_sign.visibility = View.VISIBLE
                layout_count_down_sign.visibility = View.VISIBLE
                layout_count_down_sign_success.visibility = View.INVISIBLE
            }
            ProjectConst.STATUS_SIGN_SUCCESS -> {
                layout_signin_info.visibility = View.GONE
                layout_signin_success.visibility = View.VISIBLE
                btn_sign.visibility = View.GONE
                layout_count_down_sign.visibility = View.INVISIBLE
                layout_count_down_sign_success.visibility = View.VISIBLE
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun init() {
        layout_signin_info.tv_member_name.text = signData?.name
        layout_signin_info.tv_name.text = " " + signData?.child_name + " "
        layout_signin_info.tv_member_id.text = signData?.member_id.toString()
        if (!signData?.data.isNullOrEmpty()) {
            layout_signin_info.tv_date.text = signData?.data!![0].date + ", "
            for (value in signData?.data!!) {
                if (value.sign_status != "2") {
                    checkCollections[value.id] = false
                }
            }
            if (checkCollections.isEmpty()) { //全部签到
                btn_sign.setBackgroundColor(R.mipmap.btn_disable)
                btn_sign.isEnabled = false
                isAllSignIn = true
            } else {
                btn_sign.setBackgroundResource(R.mipmap.btn_sure)
                btn_sign.isEnabled = true
                isAllSignIn = false
            }
            adapter = SignInAdapter(R.layout.item_card, signData?.data!!)
            rv_list?.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            rv_list?.adapter = adapter
            updateSignButtonView()
        }
        layout_signin_success.tv_name.text = " " + signData?.child_name + " "

        baseHandler = @SuppressLint("HandlerLeak") object : BaseHandler(this) {
            override fun handleMessage(msg: Message, what: Int) {
                if (msg.what == msgCountDown) {
                    times--
                    if (times >= 0) {
                        layout_count_down_sign.tv_count_down.text = times.toString() + "s"
                        layout_count_down_sign_success.tv_count_down.text = times.toString() + "s"
                        baseHandler?.sendEmptyMessageDelayed(msgCountDown, 1000)
                    } else {
                        val intent = Intent(BroadcastConst.ACTION_STATUS)
                        intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_HOME)
                        LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
                    }
                }
            }
        }
        baseHandler?.sendEmptyMessage(msgCountDown)
    }

    private fun initListener() {
        btn_sign.setOnClickListener {
            selectSignIds.clear()
            for ((key, value) in checkCollections) {
                if (value) {
                    selectSignIds.add(key)
                }
            }
            if (selectSignIds.isNotEmpty()) {
                baseHandler?.removeMessages(msgCountDown)
                mPresenter?.sign(qRCode, selectSignIds)
            }
        }

        ll_exit.setOnClickListener {
            val intent = Intent(BroadcastConst.ACTION_STATUS)
            intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_HOME)
            LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
        }
    }

    inner class SignInAdapter(@LayoutRes layoutResId: Int, data: List<SignInItem>) : BaseQuickAdapter<SignInItem, BaseViewHolder>(layoutResId, data) {

        override fun convert(viewHolder: BaseViewHolder, item: SignInItem) {
            val rlItemCard = viewHolder.getView<RelativeLayout>(R.id.rl_item_card)
            val tvSignInStatus = viewHolder.getView<TextView>(R.id.tv_signin_status)
            val tvLessonStatus = viewHolder.getView<TextView>(R.id.tv_lesson_status)
            val ivCheckStatus = viewHolder.getView<ImageView>(R.id.iv_check_status)
            when {
                item.sign_status == "1" -> {
                    tvSignInStatus.setTextColor(Color.parseColor("#FF847F81"))
                    ivCheckStatus.visibility = View.VISIBLE
                    ivCheckStatus.isSelected = checkCollections[item.id]!!
                    rlItemCard.setOnClickListener {
                        if (currentStatus == ProjectConst.STATUS_SIGN_IN) {
                            ivCheckStatus.isSelected = !ivCheckStatus.isSelected
                            checkCollections[item.id] = ivCheckStatus.isSelected
                        }
                        updateSignButtonView()
                    }
                }
                item.sign_status == "2" -> {
                    tvSignInStatus.setTextColor(Color.parseColor("#FFFF7B00"))
                    ivCheckStatus.visibility = View.GONE
                }
            }
            when {
                item.start_status == "1" -> {
                    tvLessonStatus.setTextColor(Color.parseColor("#FF747172"))
                    tvLessonStatus.setBackgroundResource(R.drawable.lesson_status_unstart)
                }
                item.start_status == "2" -> {
                    tvLessonStatus.setTextColor(Color.parseColor("#FF00C800"))
                    tvLessonStatus.setBackgroundResource(R.drawable.lesson_status_start)
                }
                item.start_status == "3" -> {
                    tvLessonStatus.setTextColor(Color.parseColor("#FFFF3467"))
                    tvLessonStatus.setBackgroundResource(R.drawable.lesson_status_end)
                }
            }

            viewHolder.setText(R.id.tv_time, item.start_time + " - " + item.end_time)
            when (ProjectGlobal.currentLanguage) {
                ProjectConst.LANGUAGE_CN -> {
                    viewHolder.setText(R.id.tv_lesson, item.pro_title)
                    tvSignInStatus.text = item.status_msg
                    tvLessonStatus.text = item.start_status_title
                }
                ProjectConst.LANGUAGE_EN -> {
                    viewHolder.setText(R.id.tv_lesson, item.pro_title_en)
                    tvSignInStatus.text = item.status_msg_en
                    tvLessonStatus.text = item.start_status_title_en
                }
            }
        }
    }

    private fun updateSignButtonView() {
        var hasSelect = false
        for ((_, value) in checkCollections) {
            if (value) {
                btn_sign.setBackgroundResource(R.mipmap.btn_sure)
                btn_sign.isEnabled = true
                hasSelect = true
            }
        }
        if (!hasSelect) {
            btn_sign.setBackgroundResource(R.mipmap.btn_disable)
            btn_sign.isEnabled = false
        }
    }

    override fun signSuccess() {
        currentStatus = ProjectConst.STATUS_SIGN_SUCCESS
        initLayout()
        times = 15
        baseHandler?.sendEmptyMessage(msgCountDown)
        mPresenter?.getSignInfo(qRCode, venueId)
    }

    override fun signFailed() {
        val intent = Intent(BroadcastConst.ACTION_STATUS)
        intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_ERROR)
        intent.putExtra(BroadcastConst.KEY_IS_USE_CUSTOM_ERROR_MSG, false)
        LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
    }

    override fun signInfoSuccess(data: SignInModel) {
        val newData = arrayListOf<SignInItem>()
        for (id in selectSignIds) {
            for (value in data.data) {
                if (id == value.id) {
                    value.sign_status = "2"
                    newData.add(value)
                    break
                }
            }
        }
        adapter = SignInAdapter(R.layout.item_card, newData)
        rv_list?.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        rv_list?.adapter = adapter
    }

    override fun signInfoFailed() {
        val intent = Intent(BroadcastConst.ACTION_STATUS)
        intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_ERROR)
        intent.putExtra(BroadcastConst.KEY_IS_USE_CUSTOM_ERROR_MSG, false)
        LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
    }

    @SuppressLint("SetTextI18n")
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            baseHandler?.removeMessages(msgCountDown)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (baseHandler != null) {
            baseHandler?.removeMessages(msgCountDown)
            baseHandler?.sendEmptyMessage(baseHandler!!.msgFragmentGone)
        }
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLanguageChange(event: LanguageEvent) {
        when (event.msg) {
            ProjectConst.LANGUAGE_CN -> {
                layout_signin_info.tv_welcome.text = ProjectConst.CN_WELCOME
                layout_signin_info.tv_member_id_title.text = ProjectConst.CN_MEMBER_ID
                layout_signin_info.tv_today.text = ProjectConst.CN_TODAY
                layout_signin_info.tv_name_front.text = ProjectConst.CN_FOR_YOU
                layout_signin_info.tv_name_end.text = ProjectConst.CN_FOR_YOU_LESSON
                layout_signin_info.tv_sign_in_lesson.text = ProjectConst.CN_SIGN_IN_LESSON
                layout_signin_info.tv_sign_in_lesson_hint.text = ProjectConst.CN_SIGN_IN_LESSON_HINT
                layout_signin_success.tv_name_front.text = ProjectConst.CN_FOR_YOU_LESSON_SUCCESS
                layout_signin_success.tv_name_end.text = ProjectConst.CN_SIGN_IN_LESSON_SUCCESS
                if (isAllSignIn) {
                    btn_sign.text = ProjectConst.CN_SIGN_IN_ALL
                } else {
                    btn_sign.text = ProjectConst.CN_SIGN_IN
                }
                layout_count_down_sign.tv_count_down_title.text = ProjectConst.CN_REMAINS_TIME
                layout_count_down_sign_success.tv_count_down_title.text = ProjectConst.CN_REMAINS_TIME
                tv_exit.text = ProjectConst.CN_EXIT
            }

            ProjectConst.LANGUAGE_EN -> {
                layout_signin_info.tv_welcome.text = ProjectConst.EN_WELCOME
                layout_signin_info.tv_member_id_title.text = ProjectConst.EN_MEMBER_ID
                layout_signin_info.tv_today.text = ProjectConst.EN_TODAY
                layout_signin_info.tv_name_front.text = ProjectConst.EN_FOR_YOU
                layout_signin_info.tv_name_end.text = ProjectConst.EN_FOR_YOU_LESSON
                layout_signin_info.tv_sign_in_lesson.text = ProjectConst.EN_SIGN_IN_LESSON
                layout_signin_info.tv_sign_in_lesson_hint.text = ProjectConst.EN_SIGN_IN_LESSON_HINT
                layout_signin_success.tv_name_front.text = ProjectConst.EN_FOR_YOU_LESSON_SUCCESS
                layout_signin_success.tv_name_end.text = ProjectConst.EN_SIGN_IN_LESSON_SUCCESS
                if (isAllSignIn) {
                    btn_sign.text = ProjectConst.EN_SIGN_IN_ALL
                } else {
                    btn_sign.text = ProjectConst.EN_SIGN_IN
                }
                layout_count_down_sign.tv_count_down_title.text = ProjectConst.EN_REMAINS_TIME
                layout_count_down_sign_success.tv_count_down_title.text = ProjectConst.EN_REMAINS_TIME
                tv_exit.text = ProjectConst.EN_EXIT
            }
        }
        adapter?.notifyDataSetChanged()
    }
}