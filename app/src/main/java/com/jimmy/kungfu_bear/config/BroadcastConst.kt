package com.jimmy.kungfu_bear.config

object BroadcastConst {
    const val ACTION_STATUS = "action_status"
    const val KEY_STATUS = "key_status"
    const val KEY_PLACE_NAME = "key_place_name"
    const val KEY_PLACE_NAME_EN = "key_place_name_en"
    const val KEY_PLACE_ID = "key_place_id"
    const val KEY_HOME_LIST_POSITION = "key_home_list_position"
    const val KEY_HOME_VIDEO_PROGRESS = "key_home_video_progress"
    const val KEY_IS_USE_CUSTOM_ERROR_MSG = "key_is_use_custom_error_msg"

    const val VALUE_PLACE = "main_place"
    const val VALUE_HOME = "main_home"
    const val VALUE_SIGN = "main_sign"
    const val VALUE_ERROR = "main_error"
    const val VALUE_HOME_UPDATE_TASK = "main_home_update_task"

}