package com.jimmy.kungfu_bear.network.log

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import io.realm.Sort


object LogRealmManager {
    private var realm: Realm? = null
    private var isDebug = false

    fun setMode(isDebug: Boolean) {
        this.isDebug = isDebug
    }

    fun init(context: Context, databaseName: String, version: Long, inMemory: Boolean) {
        if (isDebug) {
            Realm.init(context)
            val config = if (inMemory) {
                RealmConfiguration.Builder().name(databaseName).schemaVersion(version).inMemory().build()
            } else {
                RealmConfiguration.Builder().name(databaseName).schemaVersion(version).build()
            }
            Realm.setDefaultConfiguration(config)
        }
    }

    private fun getRealmInstance(): Realm? {
        if (isDebug) {
            if (null == realm) {
                realm = Realm.getDefaultInstance()
            }
            return realm!!
        }
        return null
    }

    fun insert(logEntity: LogEntity) {
        if (isDebug) {
            getRealmInstance()?.executeTransaction {
                it.copyToRealmOrUpdate(logEntity)
            }
        }
    }

    fun queryAllByTimeDesc(): RealmResults<LogEntity>? {
        if (isDebug) {
            val result = getRealmInstance()?.where(LogEntity::class.java)?.findAll()
            return result?.sort("startTimestamp", Sort.DESCENDING)
        }
        return null
    }

    fun queryByPrimaryKey(primaryKey: Long): LogEntity? {
        if (isDebug) {
            return getRealmInstance()?.where(LogEntity::class.java)?.equalTo("id", primaryKey)?.findFirst()
        }
        return null
    }

    fun deleteAllData() {
        if (isDebug) {
            val result = queryAllByTimeDesc()
            getRealmInstance()?.executeTransaction {
                result?.deleteAllFromRealm()
            }
        }
    }

    fun destroy() {
        if (isDebug) {
            Realm.getDefaultInstance().close()
            realm = null
        }
    }

}