package com.jimmy.kungfu_bear.callback

interface OnHomeListItemSelected {
    fun onListSelect(position: Int, isAuto: Boolean)
}

