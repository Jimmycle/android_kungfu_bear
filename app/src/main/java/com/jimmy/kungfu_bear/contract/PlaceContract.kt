package com.jimmy.kungfu_bear.contract

import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.model.PlaceModel

interface PlaceContract {
    interface View : BaseContract.IView {
        fun updateSpinner(data: List<PlaceModel>)
        fun getPlaceFailed()
    }

    interface Presenter : BaseContract.IPresenter<View> {
        fun getPlaceList()
    }

}