package com.jimmy.kungfu_bear.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.util.CommonUtil
import kotlinx.android.synthetic.main.item_banner_view.view.*
import kotlinx.android.synthetic.main.view_banner.view.*

class BannerView : FrameLayout {

    private var data: ArrayList<BannerInfo> = arrayListOf()
    private var bannerItemClick: ((BannerInfo) -> Unit)? = null
    private var bannerPageSelected: ((Int, Int) -> Unit)? = null

    var isUseIndicators: Boolean = true
    var currentPosition = 0
    var delayTimeInMills = 3000L

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    @SuppressLint("InflateParams")
    private fun init() {
        this.addView(LayoutInflater.from(context).inflate(R.layout.view_banner, null))
        auto_vp.interval = delayTimeInMills
    }

    fun setData(list: ArrayList<BannerInfo>) {
        this.data = list
        val adapter = BannerViewAdapter(context)
        auto_vp.adapter = adapter
        auto_vp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (data.isNotEmpty()) {
                    currentPosition = position % data.size
                    setCurrentIndicator()
                    bannerPageSelected?.invoke(currentPosition, position)
                }
            }
        })
        adapter.setClickItemListener {
            bannerItemClick?.invoke(it)
        }
        if (isUseIndicators) {
            container_indicator.removeAllViews()
            for (i in data.indices) {
                val dot = ImageView(context)
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                if (i != 0) {
                    params.leftMargin = CommonUtil.dip2px(context, 10f)
                }
                dot.layoutParams = params
                container_indicator.addView(dot)
            }
            setCurrentIndicator()
        }
    }

    fun setCurrentIndicator() {
        for (i in 0 until container_indicator.childCount) {
            val ivCurrent = container_indicator.getChildAt(i) as ImageView
            if (i == currentPosition) {
                ivCurrent.setBackgroundResource(R.drawable.banner_indication_focus)
            } else {
                ivCurrent.setBackgroundResource(R.drawable.banner_indication_unfocus)
            }
        }
    }

    fun startAutoScroll() {
        auto_vp.startAutoScroll(delayTimeInMills.toInt())
    }

    fun stopAutoScroll() {
        auto_vp.stopAutoScroll()
    }

    fun isAutoScrolling(): Boolean {
        return auto_vp.isAutoScrolling
    }

    fun setOnBannerItemClick(itemClickListener: (BannerInfo) -> Unit) {
        bannerItemClick = itemClickListener
    }

    fun setOnBannerPageSelected(onPageSelected: (Int, Int) -> Unit) {
        bannerPageSelected = onPageSelected
    }

    fun clearOnBannerPageSelected(){
        bannerPageSelected = null
        auto_vp.clearOnPageChangeListeners()
    }

    inner class BannerViewAdapter(var mContext: Context) : PagerAdapter() {

        private var itemClickListener: ((BannerInfo) -> Unit)? = null

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        override fun getCount(): Int {
            return Int.MAX_VALUE
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            if (`object` is View) {
                container.removeView(`object` as View?)
            }
        }

        @SuppressLint("InflateParams")
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            if (data.isEmpty()) return Any()
            val mPosition = position % data.size
            val bannerItem = LayoutInflater.from(mContext).inflate(R.layout.item_banner_view, null)
            Glide.with(mContext).load(data[mPosition].bannerUrl)
                .apply(RequestOptions().error(R.mipmap.ic_default_banner).placeholder(R.mipmap.ic_default_banner))
                .into(bannerItem.iv_banner)
            bannerItem.setOnClickListener {
                itemClickListener?.invoke(data[mPosition])
            }
            container.addView(bannerItem)
            return bannerItem
        }

        fun setClickItemListener(itemClickListener: (BannerInfo) -> Unit) {
            this.itemClickListener = itemClickListener
        }

        override fun getItemPosition(`object`: Any): Int {
            return POSITION_NONE
        }
    }

}

data class BannerInfo(
    val id: Int = 0,
    val title: String = "",
    val bannerUrl: String = "",
    val canClick: Boolean = true,
    val clickUrl: String = ""
)