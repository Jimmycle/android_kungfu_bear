package com.jimmy.kungfu_bear.util

import android.content.Context

object CommonUtil {

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    fun dip2px(context: Context, dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    /**
     * 判断是否当前是中文语言环境
     */
    fun isLanguageCN(context: Context): Boolean {
        val locale = context.resources.configuration.locale
        val language = locale.language
        if (language.endsWith("zh")) {
            return true
        }
        return false
    }
}