package com.jimmy.kungfu_bear.util

import android.util.Log


object HttpLUtil {

    private var tag = "HttpLogger"

    private const val LOG_INFO = -1

    /**
     * 日志的等级，在Application中进行全局的配置
     */
    var logLevel = LUtil.LOG_DEBUG
    var logEnable = false

    fun init(enable: Boolean, level: Int = LOG_INFO) {
        logEnable = enable
        logLevel = level
    }

    fun setEnableManual(enable: Boolean) {
        logEnable = enable
    }

    @JvmStatic
    fun tag(tag: String = "HttpLogger") {
        this.tag = tag
    }

    @JvmStatic
    fun i(msg: String) {
        if (logEnable) {
            Log.i(tag, msg)
        }
    }

}