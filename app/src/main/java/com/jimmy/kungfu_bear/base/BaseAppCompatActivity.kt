package com.jimmy.kungfu_bear.base

import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.google.gson.JsonSyntaxException
import com.jimmy.kungfu_bear.network.ConnectionLiveData
import com.trello.rxlifecycle3.components.support.RxAppCompatActivity
import retrofit2.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class BaseAppCompatActivity<P : BasePresenter<T>, T : BaseContract.IView> : RxAppCompatActivity(), BaseContract.IView {

    private val permissionRequestCode = 1099

    protected var mActivity: RxAppCompatActivity? = null
    protected var mPresenter: P? = null

    private var mPermissionCallBack: PermissionCallBack? = null

    private var progressDialog: Dialog? = null

    protected var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        ConnectionLiveData(this).observe(this, Observer {
            if (!it) {
                noNetWork()
            }
        })
        setContentView(getLayoutID())
        mPresenter = initPresenter()
        mPresenter?.setLifecycleOwner(this)
        this.savedInstanceState = savedInstanceState

        initView()
    }

    fun hideBottomUIMenu() {
        val params = window.attributes
        params.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        window.attributes = params
    }

    /**
     * 权限申请使用方法
     *
     * @param callBack      申请回调
     * @param permissions   申请权限
     *
     *
     */
    protected fun requestPermission(callBack: PermissionCallBack, @NonNull vararg permissions: String) {
        mPermissionCallBack = callBack
        if (checkPermission(*permissions)) mPermissionCallBack?.hasPermission()
        else {
            ActivityCompat.requestPermissions(this, permissions, permissionRequestCode)
        }
    }

    /**
     * 判断系统版本大于6.0的时候
     *
     * @param permissions 申请权限
     * @return 是否都已授权
     */
    private fun checkPermission(@NonNull vararg permissions: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkSelfPermissions(*permissions)
        } else true
    }

    /**
     * 检查权限是否已经授权
     *
     * @param permissions 申请权限
     * @return
     */
    private fun checkSelfPermissions(vararg permissions: String): Boolean {
        for (p in permissions) {
            if (ActivityCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        var hasAllGranted = true
        for (i in grantResults.indices) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                hasAllGranted = false
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                    mPermissionCallBack?.showPrompt()
                } else {
                    mPermissionCallBack?.lossPermission() //权限申请被拒绝 ，但用户未选择'不再提示'选项
                }
                break
            }
        }
        if (hasAllGranted) {
            mPermissionCallBack?.hasPermission()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /**
     * 权限申请回调
     */
    interface PermissionCallBack {
        fun hasPermission()
        fun lossPermission()
        fun showPrompt()
    }

    protected abstract fun getLayoutID(): Int
    protected abstract fun initPresenter(): P
    protected abstract fun initView()

    override fun showLoading(content: String) {
        try {
            if (null != mActivity) progressDialog = BaseDialog.instance.showLoadingProgressDialog(mActivity!!, content)
        } catch (e: Exception) {

        }
    }

    override fun hideLoading() {
        if (null != mActivity && null != progressDialog && progressDialog!!.isShowing && !mActivity!!.isFinishing) {
            progressDialog!!.dismiss()
        }
    }

    override fun toastSuccess(content: String) {
        try {
            if (null != mActivity) BaseDialog.instance.showToastSuccess(mActivity!!, content)
        } catch (e: Exception) {

        }
    }

    override fun toastFail(msg: String) {
        try {
            if (null != mActivity) BaseDialog.instance.showToastFail(mActivity!!, msg)
        } catch (e: Exception) {

        }
    }

    override fun toastError(e: Throwable) {
        val msg = when (e) {
            is JsonSyntaxException -> "后台数据出错"
            is UnknownHostException -> "网络异常，请检查网络连接"
            is SocketException -> "连接异常"
            is SocketTimeoutException -> "连接超时"
            is HttpException -> "网络连接出错"
            else -> e.message + ""
        }
        try {
            if (null != mActivity) BaseDialog.instance.showToastError(mActivity!!, msg)
        } catch (e: Exception) {

        }
    }

    override fun noNetWork() {
        try {
            if (null != mActivity) BaseDialog.instance.showToastTextDialog(mActivity!!, "网络异常，请检查网络连接")
        } catch (e: Exception) {

        }
    }

}