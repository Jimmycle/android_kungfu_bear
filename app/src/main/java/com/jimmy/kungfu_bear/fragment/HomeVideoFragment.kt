package com.jimmy.kungfu_bear.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jimmy.kungfu_bear.App
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.callback.OnHomeVideoItemFinished
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.config.ProjectGlobal
import com.jimmy.kungfu_bear.event.LanguageEvent
import com.jimmy.kungfu_bear.util.VolumeChangeObserver
import com.jimmy.kungfu_bear.video.IMPlayListener
import com.jimmy.kungfu_bear.video.IMPlayer
import com.jimmy.kungfu_bear.video.MPlayer
import com.jimmy.kungfu_bear.video.MPlayerException
import kotlinx.android.synthetic.main.fragment_home_video.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class HomeVideoFragment : BaseAppFragment<BasePresenter<BaseContract.IView>, BaseContract.IView>(), BaseContract.IView, VolumeChangeObserver.VolumeChangeListener {

    private var player: MPlayer? = null
    private var onHomeVideoItemFinished: OnHomeVideoItemFinished? = null
    private var isLoadingError = false
    private var isAutoFinish = false
    private var mVolumeChangeObserver: VolumeChangeObserver? = null

    override fun getLayoutID(): Int = R.layout.fragment_home_video

    override fun initPresenter(): BasePresenter<BaseContract.IView> = BasePresenter(this, this)

    companion object {
        fun newInstance(url: String, position: Int, progress: Int): HomeVideoFragment {
            val fragment = HomeVideoFragment()
            val bundle = Bundle()
            bundle.putString("url", url)
            bundle.putInt("position", position)
            bundle.putInt("progress", progress)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        init()
        mVolumeChangeObserver = VolumeChangeObserver(context!!)
        mVolumeChangeObserver?.volumeChangeListener = this
        val initVolume = mVolumeChangeObserver?.currentMusicVolume!!
        Log.d("Home", "HomeVideoFragment initVolume = $initVolume")
        val v = initVolume * 1.0f / mVolumeChangeObserver?.maxMusicVolume!!
        player?.setVolume(v)
        mVolumeChangeObserver?.registerReceiver()
        EventBus.getDefault().register(this)
    }

    override fun onVolumeChanged(volume: Int) {
        Log.d("Home", "HomeVideoFragment onVolumeChanged = $volume")
        val v = volume * 1.0f / mVolumeChangeObserver?.maxMusicVolume!!
        player?.setVolume(v)
    }

    fun init() {
        val url = arguments?.getString("url")
        val position = arguments?.getInt("position", 0)!!
        val progress = arguments?.getInt("progress", 0)!!
        if (parentFragment is HomeFragment) {
            onHomeVideoItemFinished = parentFragment as OnHomeVideoItemFinished
            if (!url.isNullOrEmpty()) {
                error_info.visibility = View.GONE
                video_play_pause.visibility = View.GONE
                val proxyUrl = App.getProxy(context!!).getProxyUrl(url)
                initPlayer(proxyUrl, position, progress)
                initListener()
            }
        }
    }

    private fun initPlayer(video_url: String, position: Int, progress: Int) {
        isLoadingError = false
        player = MPlayer()
        player?.setSource(video_url, progress)
        player?.setDisplay(mPlayerView)
        player?.setPlayListener(object : IMPlayListener {

            override fun onPrepared(player: IMPlayer) {
                player.play()
            }

            override fun onStart(player: IMPlayer) {

            }

            override fun onResume(player: IMPlayer) {

            }

            override fun onPause(player: IMPlayer) {

            }

            override fun onComplete(player: IMPlayer) {
                onHomeVideoItemFinished?.onVideoFinished(position + 1)
                isAutoFinish = true
            }

            override fun onError(error: MPlayerException) {
                isLoadingError = true
                if (error.message == "setSource error") {
                    error_info.visibility = View.VISIBLE
                    when (ProjectGlobal.currentLanguage) {
                        ProjectConst.LANGUAGE_CN -> {
                            error_info.text = ProjectConst.CN_LOADING_VIDEO_ERROR
                        }
                        ProjectConst.LANGUAGE_EN -> {
                            error_info.text = ProjectConst.EN_LOADING_VIDEO_ERROR
                        }
                    }
                }
            }
        })
    }

    private fun initListener() {
        overlay.setOnClickListener {
            if (!isLoadingError) {
                if (null != player) {
                    if (player!!.isPlaying()) {
                        player?.pause()
                        video_play_pause.visibility = View.VISIBLE
                    } else {
                        player?.resume()
                        video_play_pause.visibility = View.GONE
                    }
                } else {
                    init()
                }
            }
        }
    }

    override fun onDestroyView() {
        if (!isAutoFinish) {
            val intent = Intent(BroadcastConst.ACTION_STATUS)
            intent.putExtra(BroadcastConst.KEY_HOME_VIDEO_PROGRESS, player?.currentProgress())
            LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
        }
        Log.d("Home", "HomeVideoFragment onDestroyView")
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Home", "HomeVideoFragment onDestroy")
        player?.onDestroy()
        mVolumeChangeObserver?.unregisterReceiver()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLanguageChange(event: LanguageEvent) {
        when (event.msg) {
            ProjectConst.LANGUAGE_CN -> {
                error_info.text = ProjectConst.CN_LOADING_VIDEO_ERROR
            }
            ProjectConst.LANGUAGE_EN -> {
                error_info.text = ProjectConst.EN_LOADING_VIDEO_ERROR
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("Home", "HomeVideoFragment onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("Home", "HomeVideoFragment onPause")
    }

}