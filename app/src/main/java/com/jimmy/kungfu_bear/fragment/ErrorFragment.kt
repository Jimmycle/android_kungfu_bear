package com.jimmy.kungfu_bear.fragment

import android.content.Intent
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.config.ProjectGlobal
import com.jimmy.kungfu_bear.event.LanguageEvent
import kotlinx.android.synthetic.main.fragment_error.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * 错误信息
 */
class ErrorFragment : BaseAppFragment<BasePresenter<BaseContract.IView>, BaseContract.IView>(), BaseContract.IView {

    var errorMsgCN: String? = ""
    var errorMsgEN: String? = ""

    override fun getLayoutID(): Int = R.layout.fragment_error

    override fun initPresenter(): BasePresenter<BaseContract.IView> = BasePresenter(this, this)

    companion object {
        fun newInstance(errorMsgCN: String = "", errorMsgEN: String = ""): ErrorFragment {
            val fragment = ErrorFragment()
            val bundle = Bundle()
            bundle.putString("errorMsgCN", errorMsgCN)
            bundle.putString("errorMsgEN", errorMsgEN)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        errorMsgCN = arguments?.getString("errorMsgCN")
        errorMsgEN = arguments?.getString("errorMsgEN")

        onLanguageChange(LanguageEvent(ProjectGlobal.currentLanguage))
        ll_exit.setOnClickListener {
            val intent = Intent(BroadcastConst.ACTION_STATUS)
            intent.putExtra(BroadcastConst.KEY_STATUS, BroadcastConst.VALUE_HOME)
            LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
        }
        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLanguageChange(event: LanguageEvent) {
        when (event.msg) {
            ProjectConst.LANGUAGE_CN -> {
                if (errorMsgCN.isNullOrEmpty()) {
                    tv_hint.text = ProjectConst.CN_SYSTEM_ERROR
                } else {
                    tv_hint.text = errorMsgCN
                }
                tv_exit.text = ProjectConst.CN_EXIT
            }

            ProjectConst.LANGUAGE_EN -> {
                if (errorMsgEN.isNullOrEmpty()) {
                    tv_hint.text = ProjectConst.EN_SYSTEM_ERROR
                } else {
                    tv_hint.text = errorMsgEN
                }
                tv_exit.text = ProjectConst.EN_EXIT
            }
        }
    }

}