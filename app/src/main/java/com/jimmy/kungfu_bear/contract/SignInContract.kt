package com.jimmy.kungfu_bear.contract

import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.model.SignInModel
import okhttp3.RequestBody

interface SignInContract {
    interface View : BaseContract.IView {
        fun signSuccess()
        fun signFailed()
        fun signInfoSuccess(data: SignInModel)
        fun signInfoFailed()
    }

    interface Presenter : BaseContract.IPresenter<View> {
        fun sign(code: String, id: List<Int>)
        fun getSignInfo(code: String, venue_id: Int)
    }

}