package com.jimmy.kungfu_bear.presenter

import com.google.gson.Gson
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.contract.MainContract
import com.jimmy.kungfu_bear.network.BearApi
import com.jimmy.kungfu_bear.util.SpUtil
import com.trello.rxlifecycle3.components.support.RxAppCompatActivity

class MainPresenter(activity: RxAppCompatActivity, view: MainContract.View) : BasePresenter<MainContract.View>(activity, view), MainContract.Presenter {

    override fun getSignInfo(code: String, venue_id: Int) {
        BearApi.getService(mActivity!!).getSignInfo(code, venue_id).execute {
            onNext {
                mView?.hideLoading()
                if (it.hasData) {
                    mView?.signInfoSuccess(it.data!!)
                } else {
                    mView?.signInfoFailed()
                }
            }
            onError {
                mView?.hideLoading()
                mView?.signInfoFailed()
            }
        }
    }

    override fun getHomeData() {
        BearApi.getService(mActivity!!).getHomeData().execute {
            onNext {
                if (it.hasData) {
                    val jsonHomeCacheData = SpUtil.open(mActivity!!).getSpData(ProjectConst.SP_HOME_DATA, "")
                    if (Gson().toJson(it.data) != jsonHomeCacheData) {
                        SpUtil.open(mActivity!!).putSpData(ProjectConst.SP_HOME_DATA, Gson().toJson(it.data))
                    }
                }
            }

            onError {

            }
        }
    }

}