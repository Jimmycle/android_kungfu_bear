package com.jimmy.kungfu_bear.view;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jimmy.kungfu_bear.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class DigitalClockView extends LinearLayout {

    private TextView tvTime;
    private boolean mAttached = false;

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onTimeChanged();
            invalidate();
        }
    };

    public DigitalClockView(Context context) {
        this(context, null);
    }

    public DigitalClockView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DigitalClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.view_digital_clock, this);
        tvTime = findViewById(R.id.tv_time);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!mAttached) {
            mAttached = true;
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_TIME_TICK);
            filter.addAction(Intent.ACTION_TIME_CHANGED);
            filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
            getContext().registerReceiver(mIntentReceiver, filter, null, null);
        }
        onTimeChanged();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAttached) {
            getContext().unregisterReceiver(mIntentReceiver);
            mAttached = false;
        }
    }

    private void onTimeChanged() {
        updateTime();
    }

    private void updateTime() {
        final Calendar myCalendar = Calendar.getInstance(TimeZone.getDefault());
        myCalendar.setTimeInMillis(System.currentTimeMillis());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String time = sdf.format(myCalendar.getTime());
        tvTime.setText(time);
    }
}
