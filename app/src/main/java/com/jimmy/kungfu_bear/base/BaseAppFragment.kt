package com.jimmy.kungfu_bear.base

import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.google.gson.JsonSyntaxException
import com.jimmy.kungfu_bear.network.ConnectionLiveData
import com.trello.rxlifecycle3.components.support.RxFragment
import retrofit2.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseAppFragment<P : BasePresenter<T>, T : BaseContract.IView> : RxFragment(), BaseContract.IView {

    private val permissionRequestCode = 1666

    protected var mPresenter: P? = null
    private var mPermissionCallBack: PermissionCallBack? = null

    private var progressDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (null != activity) {
            ConnectionLiveData(activity!!).observe(this, Observer {
                if (!it) {
                    noNetWork()
                }
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutID(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter = initPresenter()
        mPresenter?.setLifecycleOwner(this)

        initView()
    }

    /**
     * 权限申请使用方法
     *
     * @param callBack      申请回调
     * @param permissions   申请权限
     *
     *
     */
    protected fun requestPermission(callBack: PermissionCallBack, @NonNull vararg permissions: String) {
        mPermissionCallBack = callBack
        if (checkPermission(*permissions))
            mPermissionCallBack?.hasPermission()
        else {
            if (null != activity) {
                ActivityCompat.requestPermissions(activity!!, permissions, permissionRequestCode)
            }
        }
    }

    /**
     * 判断系统版本大于6.0的时候
     *
     * @param permissions 申请权限
     * @return 是否都已授权
     */
    private fun checkPermission(@NonNull vararg permissions: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkSelfPermissions(*permissions)
        } else true
    }

    /**
     * 检查权限是否已经授权
     *
     * @param permissions 申请权限
     * @return
     */
    private fun checkSelfPermissions(vararg permissions: String): Boolean {
        for (p in permissions) {
            if (null != activity && ActivityCompat.checkSelfPermission(
                    activity!!,
                    p
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        var hasAllGranted = true
        for (i in grantResults.indices) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                hasAllGranted = false
                if (null != activity && !ActivityCompat.shouldShowRequestPermissionRationale(
                        activity!!,
                        permissions[i]
                    )
                ) {
                    mPermissionCallBack?.showPrompt()
                } else {
                    mPermissionCallBack?.lossPermission() //权限申请被拒绝 ，但用户未选择'不再提示'选项
                }
                break
            }
        }
        if (hasAllGranted) {
            mPermissionCallBack?.hasPermission()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /**
     * 权限申请回调
     */
    interface PermissionCallBack {
        fun hasPermission()
        fun lossPermission()
        fun showPrompt()
    }

    protected abstract fun getLayoutID(): Int
    protected abstract fun initPresenter(): P
    protected abstract fun initView()

    override fun showLoading(content: String) {
        try {
            if (null != activity)
                progressDialog = BaseDialog.instance.showLoadingProgressDialog(activity!!, content)
        } catch (e: Exception) {

        }
    }

    override fun hideLoading() {
        if (null != progressDialog && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    override fun toastSuccess(content: String) {
        try {
            if (null != activity)
                BaseDialog.instance.showToastSuccess(activity!!, content)
        } catch (e: Exception) {

        }
    }

    override fun toastFail(msg: String) {
        try {
            if (null != activity)
                BaseDialog.instance.showToastFail(activity!!, msg)
        } catch (e: Exception) {

        }
    }

    override fun toastError(e: Throwable) {
        val msg = when (e) {
            is JsonSyntaxException -> "后台数据出错"
            is UnknownHostException -> "网络异常，请检查网络连接"
            is SocketException -> "连接异常"
            is SocketTimeoutException -> "连接超时"
            is HttpException -> "网络连接出错"
            else -> e.message + ""
        }
        try {
            if (null != activity)
                BaseDialog.instance.showToastError(activity!!, msg)
        } catch (e: Exception) {

        }
    }

    override fun noNetWork() {
        try {
            if (null != activity)
                BaseDialog.instance.showToastTextDialog(activity!!, "网络异常，请检查网络连接")
        } catch (e: Exception) {

        }
    }

}