package com.jimmy.kungfu_bear.util

import android.util.Log

object LUtil {
    private var tag = "LUtil"

    const val LOG_ERROR = 0
    const val LOG_WARNING = 1
    const val LOG_INFO = 2
    const val LOG_DEBUG = 3

    /**
     * 日志的等级，在Application中进行全局的配置
     */
    var logLevel = LOG_DEBUG
    var logEnable = true

    fun init(enable: Boolean, level: Int = LOG_DEBUG) {
        logLevel = level
        logEnable = enable
    }

    @JvmStatic
    fun tag(clazz: Class<*>) {
        tag = clazz.simpleName
    }

    @JvmStatic
    fun tag(tag: String) {
        this.tag = tag
    }

    @JvmStatic
    fun e(msg: String) {
        if (logEnable && LOG_ERROR <= logLevel && msg.isNotBlank()) {
            Log.e(tag, msg)
        }
    }

    @JvmStatic
    fun w(msg: String) {
        if (logEnable && LOG_WARNING <= logLevel && msg.isNotBlank()) {
            Log.e(tag, msg)
        }
    }

    @JvmStatic
    fun i(msg: String) {
        if (logEnable && LOG_INFO <= logLevel && msg.isNotBlank()) {
            Log.i(tag, msg)
        }
    }

    @JvmStatic
    fun d(msg: String) {
        if (logEnable && LOG_DEBUG <= logLevel && msg.isNotBlank()) {
            Log.d(tag, msg)
        }
    }

}