package com.jimmy.kungfu_bear.presenter

import com.google.gson.Gson
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.contract.HomeContract
import com.jimmy.kungfu_bear.model.HomeModel
import com.jimmy.kungfu_bear.network.BearApi
import com.jimmy.kungfu_bear.util.SpUtil
import com.trello.rxlifecycle3.components.support.RxFragment

class HomePresenter(fragment: RxFragment, view: HomeContract.View) : BasePresenter<HomeContract.View>(fragment, view), HomeContract.Presenter {
    override fun getHomeData() {
        val jsonHomeCacheData = SpUtil.open(mFragment?.activity!!).getSpData(ProjectConst.SP_HOME_DATA, "")
        if (jsonHomeCacheData.isNotEmpty()) {
            mView?.initHomeView(Gson().fromJson(jsonHomeCacheData, HomeModel::class.java))
        } else {
            BearApi.getService(mFragment?.activity!!).getHomeData().execute {
                onNext {
                    if (it.hasData) {
                        if (Gson().toJson(it.data) != jsonHomeCacheData) {
                            SpUtil.open(mFragment?.activity!!).putSpData(ProjectConst.SP_HOME_DATA, Gson().toJson(it.data))
                        }
                        mView?.initHomeView(it.data)
                    } else {
                        if (jsonHomeCacheData.isNotEmpty()) {
                            mView?.initHomeView(Gson().fromJson(jsonHomeCacheData, HomeModel::class.java))
                        }
                    }
                }

                onError {
                    if (jsonHomeCacheData.isNotEmpty()) {
                        mView?.initHomeView(Gson().fromJson(jsonHomeCacheData, HomeModel::class.java))
                    }
                }
            }
        }
    }

}