package com.jimmy.kungfu_bear.video

interface IMPlayListener {

    fun onPrepared(player: IMPlayer)
    fun onStart(player: IMPlayer)
    fun onResume(player: IMPlayer)
    fun onPause(player: IMPlayer)
    fun onComplete(player: IMPlayer)
    fun onError(error:MPlayerException)

}