package com.jimmy.kungfu_bear.callback

interface OnHomeImageItemFinished {
    fun onImageFinished(position: Int)
}

