package com.jimmy.kungfu_bear.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import org.jetbrains.annotations.NotNull


interface BaseContract {

    interface IPresenter<T : IView> : LifecycleObserver {
        fun setLifecycleOwner(lifecycleOwner: LifecycleOwner)

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onCreate(@NotNull owner: LifecycleOwner)

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy(@NotNull owner: LifecycleOwner)

        @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
        fun onLifecycleChanged(@NotNull owner: LifecycleOwner, @NotNull event: Lifecycle.Event)
    }

    interface IView {
        /**
         * 显示进度
         */
        fun showLoading(content: String)

        /**
         * 隐藏进度
         */
        fun hideLoading()

        /**
         * 弹出成功提示
         */
        fun toastSuccess(content: String)

        /**
         * 弹出失败提示
         */
        fun toastFail(msg: String)

        /**
         * 弹出错误提示
         */
        fun toastError(e: Throwable)

        /**
         * 网络不可用
         */
        fun noNetWork()
    }

}