package com.jimmy.kungfu_bear.contract

import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.model.HomeModel

interface HomeContract {
    interface View : BaseContract.IView {
        fun initHomeView(data: HomeModel?)
    }

    interface Presenter : BaseContract.IPresenter<View> {
        fun getHomeData()
    }

}