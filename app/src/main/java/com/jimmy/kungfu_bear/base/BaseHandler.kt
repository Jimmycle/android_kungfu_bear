package com.jimmy.kungfu_bear.base

import android.app.Activity
import android.os.Handler
import android.os.Message
import android.util.Log
import androidx.fragment.app.Fragment
import java.lang.ref.WeakReference


abstract class BaseHandler private constructor() : Handler() {
    private val tag = "BaseHandler"

    val msgActivityGone: Int = -1
    val msgFragmentGone: Int = -2

    var activityWeakReference: WeakReference<Activity>? = null
    var fragmentWeakReference: WeakReference<Fragment>? = null

    constructor(activity: Activity) : this() {
        this.activityWeakReference = WeakReference(activity)
    }

    constructor(fragment: Fragment) : this() {
        this.fragmentWeakReference = WeakReference(fragment)
    }

    override fun handleMessage(msg: Message) {
        if (activityWeakReference != null) {
            if (activityWeakReference!!.get() == null || activityWeakReference!!.get()!!.isFinishing || msg.what == msgActivityGone) {
                Log.d(tag, "Activity is gone")
                this.removeCallbacksAndMessages(null)
            } else {
                handleMessage(msg, msg.what)
            }
        } else if (fragmentWeakReference != null) {
            if (fragmentWeakReference!!.get() == null || fragmentWeakReference!!.get()!!.isRemoving || msg.what == msgFragmentGone) {
                Log.d(tag, "Fragment is gone")
                this.removeCallbacksAndMessages(null)
            } else {
                handleMessage(msg, msg.what)
            }
        } else {
            handleMessage(msg, msg.what)
        }
    }

    abstract fun handleMessage(msg: Message, what: Int)

}