package com.jimmy.kungfu_bear.fragment

import android.os.Bundle
import android.util.Log
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.callback.OnHomeImageItemFinished
import com.jimmy.kungfu_bear.view.BannerInfo
import kotlinx.android.synthetic.main.fragment_home_image.*

class HomeImageFragment : BaseAppFragment<BasePresenter<BaseContract.IView>, BaseContract.IView>(), BaseContract.IView {

    private var onHomeImageItemFinished: OnHomeImageItemFinished? = null

    override fun getLayoutID(): Int = R.layout.fragment_home_image

    override fun initPresenter(): BasePresenter<BaseContract.IView> = BasePresenter(this, this)

    companion object {
        fun newInstance(list: ArrayList<String>, position: Int): HomeImageFragment {
            val fragment = HomeImageFragment()
            val bundle = Bundle()
            bundle.putStringArrayList("list", list)
            bundle.putInt("position", position)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        init()
    }

    private fun init() {
        val list = arguments?.getStringArrayList("list")
        val position = arguments?.getInt("position", 0)!!
        if (parentFragment is HomeFragment) {
            onHomeImageItemFinished = parentFragment as OnHomeImageItemFinished
            val bannerInfo = arrayListOf<BannerInfo>()
            if (!list.isNullOrEmpty()) {
                for (idx in list.indices) {
                    val info = BannerInfo(
                        id = idx, title = "", bannerUrl = list[idx], canClick = false, clickUrl = ""
                    )
                    bannerInfo.add(info)
                }
                banner_view.isUseIndicators = true
                banner_view.setData(bannerInfo)
                if (!banner_view.isAutoScrolling()) {
                    banner_view.startAutoScroll()
                }
                banner_view.setOnBannerPageSelected { currentPosition, totalPosition ->
                    if (currentPosition == 0 && totalPosition >= list.size) {
                        onHomeImageItemFinished?.onImageFinished(position + 1)
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        banner_view.stopAutoScroll()
        banner_view.clearOnBannerPageSelected()
        super.onDestroyView()
        Log.d("Home", "HomeImageFragment onDestroyView")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Home", "HomeImageFragment onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("Home", "HomeImageFragment onPause")
    }

}