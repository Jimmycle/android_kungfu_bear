package com.jimmy.kungfu_bear.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.google.gson.Gson
import com.jimmy.kungfu_bear.R
import com.jimmy.kungfu_bear.base.BaseAppFragment
import com.jimmy.kungfu_bear.base.BaseContract
import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.callback.OnHomeListItemSelected
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.config.ProjectGlobal
import com.jimmy.kungfu_bear.event.LanguageEvent
import com.jimmy.kungfu_bear.model.HomeItem
import com.jimmy.kungfu_bear.model.HomeModel
import kotlinx.android.synthetic.main.fragment_home_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class HomeListFragment : BaseAppFragment<BasePresenter<BaseContract.IView>, BaseContract.IView>(), BaseContract.IView {

    private var homeData: HomeModel? = null
    private var adapter: MyAdapter? = null
    private var onHomeLIstItemSelected: OnHomeListItemSelected? = null

    override fun getLayoutID(): Int = R.layout.fragment_home_list

    override fun initPresenter(): BasePresenter<BaseContract.IView> = BasePresenter(this, this)

    companion object {
        fun newInstance(homeList: HomeModel, position: Int): HomeListFragment {
            val fragment = HomeListFragment()
            val bundle = Bundle()
            bundle.putString("list", Gson().toJson(homeList))
            bundle.putInt("position", position)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView() {
        init()
        EventBus.getDefault().register(this)
    }

    fun init() {
        homeData = Gson().fromJson(arguments?.getString("list", ""), HomeModel::class.java)
        val position = arguments?.getInt("position", 0)!!
        if (parentFragment is HomeFragment) {
            onHomeLIstItemSelected = parentFragment as OnHomeListItemSelected
            when (ProjectGlobal.currentLanguage) {
                ProjectConst.LANGUAGE_CN -> {
                    tv_list_title?.text = homeData?.title
                }
                ProjectConst.LANGUAGE_EN -> {
                    tv_list_title?.text = homeData?.title_en
                }
            }
            if (!homeData?.list.isNullOrEmpty()) {
                adapter = MyAdapter(R.layout.item_home_list, homeData?.list!!)
                rv_list?.layoutManager = LinearLayoutManager(context)
                rv_list?.adapter = adapter
            }
            adapter?.selectedPosition(position, true)
            adapter?.let {
                it.setOnItemClickListener { _, _, _position ->
                    it.selectedPosition(_position, false)
                }
            }
        }
    }

    fun refreshList(position: Int) {
        adapter?.selectedPosition(position, true)
    }

    inner class MyAdapter(@LayoutRes layoutResId: Int, data: List<HomeItem>) : BaseQuickAdapter<HomeItem, BaseViewHolder>(layoutResId, data) {
        var position: Int = -1

        override fun convert(viewHolder: BaseViewHolder, item: HomeItem) {
            val tvContent = viewHolder.getView<TextView>(R.id.tv_content)
            when (ProjectGlobal.currentLanguage) {
                ProjectConst.LANGUAGE_CN -> {
                    tvContent.text = item.title
                }
                ProjectConst.LANGUAGE_EN -> {
                    tvContent.text = item.title_en
                }
            }
            if (viewHolder.adapterPosition == position) {
                tvContent.setTextColor(Color.parseColor("#FFFF5700"))
                tvContent.textSize = 26f
            } else {
                tvContent.setTextColor(Color.parseColor("#FFFFFFFF"))
                tvContent.textSize = 20f
            }

        }

        fun selectedPosition(position: Int, isAuto: Boolean) {
            this.position = position
            notifyDataSetChanged()
            onHomeLIstItemSelected?.onListSelect(position, isAuto)
        }
    }

    override fun onDestroyView() {
        Log.d("Home", "HomeListFragment onDestroyView")
        val intent = Intent(BroadcastConst.ACTION_STATUS)
        intent.putExtra(BroadcastConst.KEY_HOME_LIST_POSITION, adapter?.position)
        LocalBroadcastManager.getInstance(activity!!).sendBroadcastSync(intent)
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLanguageChange(event: LanguageEvent) {
        if (isAdded) {
            when (event.msg) {
                ProjectConst.LANGUAGE_CN -> {
                    tv_list_title.text = homeData?.title
                }

                ProjectConst.LANGUAGE_EN -> {
                    tv_list_title.text = homeData?.title_en
                }
            }
            adapter?.notifyDataSetChanged()
        }
    }

}