package com.jimmy.kungfu_bear.video

import android.content.res.AssetFileDescriptor
import android.media.MediaPlayer
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import java.io.IOException

class MPlayer : IMPlayer, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnPreparedListener,
    MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnErrorListener, SurfaceHolder.Callback {

    private val tag = "MPlayer"

    private var player: MediaPlayer? = null
    private var display: SurfaceView? = null
    private var mPlayListener: IMPlayListener? = null
    private var source: String? = null
    private var sourceAssetFd: AssetFileDescriptor? = null
    private var isLooping = false

    private var isVideoSizeMeasured = false  //视频宽高是否已获取，且不为0
    private var isMediaPrepared = false      //视频资源是否准备完成
    private var isSurfaceCreated = false     //Surface是否被创建
    private var isResumed = false            //是否在Resume状态
    private var mIsCrop = false
    private var currentVideoWidth: Int = 0              //当前视频宽度
    private var currentVideoHeight: Int = 0             //当前视频高度
    private var position = 0
    private var isSeekComplete = false //播放完毕

    /**
     * 检查是否需要重新创建资源
     */
    private fun createPlayerIfNeed() {
        if (null == player) {
            player = MediaPlayer()
            player?.setScreenOnWhilePlaying(true)
            player?.setOnBufferingUpdateListener(this)
            player?.setOnVideoSizeChangedListener(this)
            player?.setOnCompletionListener(this)
            player?.setOnPreparedListener(this)
            player?.setOnSeekCompleteListener(this)
            player?.setOnErrorListener(this)
        }
    }

    /**
     * 开始播放
     */
    private fun playStart() {
        Log.d(tag, "playStart: ----------->")
        if (isMediaPrepared && isSurfaceCreated) {
            player?.setDisplay(display?.holder)
            player?.seekTo(position)
            player?.start()
            player?.isLooping = isLooping
            if (mPlayListener != null) {
                mPlayListener?.onStart(this)
            }
        }
    }

    /**
     * 暂停播放
     */
    private fun playPause() {
        Log.d(tag, "playPause: ----------->")
        if (isPlaying()) {
            player?.pause()
            if (mPlayListener != null) {
                mPlayListener?.onPause(this)
            }
        }
    }

    /**
     * 继续播放
     */
    private fun playResume() {
        Log.d(tag, "playResume: ----------->")
        if (player != null && !player!!.isPlaying) {
            player?.start()
            if (mPlayListener != null) {
                mPlayListener?.onResume(this)
            }
        }
    }

    /**
     * 检测资源是否合法
     */
    private fun canPlay(): Boolean {
        return !source.isNullOrEmpty() || sourceAssetFd != null
    }

    /**
     * 设置是否裁剪视频，若裁剪，则视频按照DisplayView的父布局大小显示。
     * 若不裁剪，视频居中于DisplayView的父布局显示
     *
     * @param isCrop 是否裁剪视频
     */
    fun setCrop(isCrop: Boolean) {
        this.mIsCrop = isCrop
        if (display != null && currentVideoWidth > 0 && currentVideoHeight > 0) {
            tryResetSurfaceSize(display!!, currentVideoWidth, currentVideoHeight)
        }
    }

    fun isCrop(): Boolean {
        return mIsCrop
    }

    fun setDisplay(display: SurfaceView) {
        if (this.display != null && this.display?.holder != null) {
            this.display?.holder?.removeCallback(this)
        }
        this.display = display
        this.display?.holder?.addCallback(this)
    }

    fun isPlaying(): Boolean {
        return player != null && player!!.isPlaying
    }

    fun currentProgress(): Int {
        if (player != null) {
            return player!!.currentPosition
        }
        return 0
    }

    fun setVolume(volume: Float) {
        if (player != null) {
            player!!.setVolume(volume, volume)
        }
    }

    //根据设置和视频尺寸，调整视频播放区域的大小
    private fun tryResetSurfaceSize(view: View, videoWidth: Int, videoHeight: Int) {
        val parent = view.parent as ViewGroup
        val width = parent.width
        val height = parent.height
        if (width > 0 && height > 0) {
            val params = view.layoutParams as FrameLayout.LayoutParams
            if (mIsCrop) {
                val scaleVideo = videoWidth / videoHeight.toFloat()
                val scaleSurface = width / height.toFloat()
                if (scaleVideo < scaleSurface) {
                    params.width = width
                    params.height = (width / scaleVideo).toInt()
                    params.setMargins(0, (height - params.height) / 2, 0, (height - params.height) / 2)
                } else {
                    params.height = height
                    params.width = (height * scaleVideo).toInt()
                    params.setMargins((width - params.width) / 2, 0, (width - params.width) / 2, 0)
                }
            } else {
                if (videoWidth > width || videoHeight > height) {
                    val scaleVideo = videoWidth / videoHeight.toFloat()
                    val scaleSurface = (width / height).toFloat()
                    if (scaleVideo > scaleSurface) {
                        params.width = width
                        params.height = (width / scaleVideo).toInt()
                        params.setMargins(0, (height - params.height) / 2, 0, (height - params.height) / 2)
                    } else {
                        params.height = height
                        params.width = (height * scaleVideo).toInt()
                        params.setMargins((width - params.width) / 2, 0, (width - params.width) / 2, 0)
                    }
                }
            }
            view.layoutParams = params
        }
    }

    fun setPlayListener(listener: IMPlayListener) {
        this.mPlayListener = listener
    }

    override fun setSource(url: String, position: Int) {
        this.source = url
        this.position = position
        if (!canPlay()) {
            mPlayListener?.onError(MPlayerException("setSource error", null))
//            throw MPlayerException("Please setSource", null)
        }
        createPlayerIfNeed()
        isMediaPrepared = false
        isVideoSizeMeasured = false
        currentVideoWidth = 0
        currentVideoHeight = 0
        player?.reset()
        try {
            player?.setDataSource(url)
            player?.prepareAsync()
        } catch (e: IOException) {
            Log.d(tag, "setSource: ------------->" + e.message)
            mPlayListener?.onError(MPlayerException("setSource error", e))
//            throw MPlayerException("set source error", e)
        }
    }

    override fun setSourceAssetFd(assetFd: AssetFileDescriptor, position: Int) {
        this.sourceAssetFd = assetFd
        this.position = position
        if (!canPlay()) {
            mPlayListener?.onError(MPlayerException("setSource error", null))
//            throw MPlayerException("Please setSource", null)
        }
        createPlayerIfNeed()
        isMediaPrepared = false
        isVideoSizeMeasured = false
        currentVideoWidth = 0
        currentVideoHeight = 0
        player?.reset()
        try {
            player?.setDataSource(assetFd.fileDescriptor, assetFd.startOffset, assetFd.length)
            player?.prepareAsync()
            Log.d(tag, "setSource success")
        } catch (e: IOException) {
            Log.d(tag, "setSource: ------------->" + e.message)
            mPlayListener?.onError(MPlayerException("setSource error", e))
//            throw MPlayerException("set source error", e)
        }
    }

    override fun play() {
        playStart()
    }

    override fun pause() {
        playPause()
    }

    override fun resume() {
        playResume()
    }

    override fun onResume() {
        if (isPlaying()) {
            isResumed = true
        }
    }

    override fun onPause() {
        if (!isPlaying()) {
            isResumed = false
        }
    }

    override fun onDestroy() {
        if (player != null) {
            player?.release()
        }
    }

    override fun onBufferingUpdate(mp: MediaPlayer?, percent: Int) {

    }

    override fun onCompletion(mp: MediaPlayer?) {
        Log.d(tag, "onCompletion: -------------->")
        if (mPlayListener != null) {
            mPlayListener?.onComplete(this)
        }
    }

    override fun onVideoSizeChanged(mp: MediaPlayer?, width: Int, height: Int) {
        Log.d(tag, "onVideoSizeChanged: -------------->$width/$height")
        if (width > 0 && height > 0) {
            this.currentVideoWidth = width
            this.currentVideoHeight = height
//            tryResetSurfaceSize(display!!, width, height)
            isVideoSizeMeasured = true
        }
    }

    override fun onPrepared(mp: MediaPlayer?) {
        Log.d(tag, "onPrepared: -------------->")
        isMediaPrepared = true
        if (mPlayListener != null) {
            mPlayListener?.onPrepared(this)
        }
    }

    override fun onSeekComplete(mp: MediaPlayer?) {
        Log.d(tag, "onSeekComplete: --------->")
        isSeekComplete = true
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        Log.d(tag, "onError: ---------> $mp ~~ $what ~~ $extra~~")
        mPlayListener?.onError(MPlayerException("setSource error", null))
        return true
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        Log.d(tag, "surfaceChanged: ----------->")
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        Log.d(tag, "surfaceDestroyed: ----------->")
        if (display != null && holder === display?.holder) {
            isSurfaceCreated = false
            try {
                position = 0
                player?.release()
                player = null
                source = null
                sourceAssetFd = null
            } catch (e: MPlayerException) {
                e.printStackTrace()
            }
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        Log.d(tag, "surfaceCreated: ----------->" + (player != null))
        if (display != null && holder === display?.holder) {
            isSurfaceCreated = true
        }
    }

}