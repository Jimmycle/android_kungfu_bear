package com.jimmy.kungfu_bear.network.log

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class LogEntity : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    // 请求url
    var requestUrl: String? = ""
    // 请求方式
    var requestMethod: String = "GET"
    // 请求协议
    var requestProtocol: String = "http/1.1"
    // 请求Header
    var requestHeader: String? = ""
    // 请求Body
    var requestBody: String? = ""
    var requestBodySize: Long = 0
    // 结果码
    var responseCode: Int = 0
    // 结果Header
    var responseHeader: String? = ""
    // 结果Body
    var responseBody: String? = ""
    var responseBodySize: Long = 0
    // 请求发起时间(时间戳)
    var startTimestamp: Long = 0
    // 请求时间(单位ms)
    var useTime: Long = 0

}