package com.jimmy.kungfu_bear

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Message
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jimmy.kungfu_bear.base.BaseAppCompatActivity
import com.jimmy.kungfu_bear.base.BaseHandler
import com.jimmy.kungfu_bear.config.BroadcastConst
import com.jimmy.kungfu_bear.config.ProjectConst
import com.jimmy.kungfu_bear.config.ProjectGlobal
import com.jimmy.kungfu_bear.contract.MainContract
import com.jimmy.kungfu_bear.event.LanguageEvent
import com.jimmy.kungfu_bear.fragment.ErrorFragment
import com.jimmy.kungfu_bear.fragment.HomeFragment
import com.jimmy.kungfu_bear.fragment.PlaceFragment
import com.jimmy.kungfu_bear.fragment.SignInFragment
import com.jimmy.kungfu_bear.manager.ScanKeyManager
import com.jimmy.kungfu_bear.model.SignInModel
import com.jimmy.kungfu_bear.presenter.MainPresenter
import com.jimmy.kungfu_bear.util.AlarmReceiver
import com.jimmy.kungfu_bear.util.SpUtil
import com.jimmy.kungfu_bear.util.addFragment
import com.jimmy.kungfu_bear.util.replaceFragment
import kotlinx.android.synthetic.main.common_title_bar.*
import org.greenrobot.eventbus.EventBus
import java.util.*


class MainActivity : BaseAppCompatActivity<MainPresenter, MainContract.View>(), MainContract.View {

    //全局广播,页面状态
    private lateinit var statusReceiver: BroadcastReceiver
    private var baseHandler: BaseHandler? = null
    private var msgExitCountReset = 0x88
    private var scanKeyManager: ScanKeyManager? = null
    private val fragments = arrayListOf<Fragment>()
    private val placeFragment = PlaceFragment()
    private var homeFragment = HomeFragment()
    private var signInFragment = SignInFragment()
    private var errorFragment = ErrorFragment()
    private var getQRCode = ""
    private var venueId = -1
    private var signInModel: SignInModel? = null
    private var placeName = ""
    private var placeNameEN = ""
    private var exitCount = 0
    private var errorMsgCN = ""
    private var errorMsgEN = ""
    private var homeListPosition = 0
    private var homeVideoProgress = 0
    private var isUseCustomErrorMsg = false

    override fun getLayoutID(): Int = R.layout.activity_main

    override fun initPresenter(): MainPresenter = MainPresenter(this, this)

    @SuppressLint("SetTextI18n")
    override fun initView() {
        hideBottomUIMenu()
        init()
        initTitle()
        initListener()
        initBroadcast()
        mPresenter?.getHomeData()
        showFragmentByStatus(BroadcastConst.VALUE_PLACE)
        setTimerTask()
    }

    private fun init() {
        ProjectGlobal.currentLanguage = SpUtil.open(this).getSpData(ProjectConst.SP_LANGUAGE, ProjectConst.LANGUAGE_CN)
        fragments.add(placeFragment)
        fragments.add(homeFragment)
        fragments.add(signInFragment)
        fragments.add(errorFragment)
    }

    @SuppressLint("SetTextI18n")
    private fun initTitle() {
        when (ProjectGlobal.currentLanguage) {
            ProjectConst.LANGUAGE_CN -> {
                iv_change_language.setImageResource(R.mipmap.ic_language_cn)
                if (placeName.isEmpty()) {
                    tv_title.text = ProjectConst.CN_TITLE
                } else {
                    tv_title.text = ProjectConst.CN_TITLE + " —— " + placeName
                }
            }
            ProjectConst.LANGUAGE_EN -> {
                iv_change_language.setImageResource(R.mipmap.ic_language_en)
                if (placeNameEN.isEmpty()) {
                    tv_title.text = ProjectConst.EN_TITLE
                } else {
                    tv_title.text = ProjectConst.EN_TITLE + " —— " + placeNameEN
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initListener() {
        baseHandler = @SuppressLint("HandlerLeak") object : BaseHandler(this) {
            override fun handleMessage(msg: Message, what: Int) {
                if (msg.what == msgExitCountReset) {
                    exitCount = 0
                }
            }
        }

        iv_change_language.setOnClickListener {
            when (ProjectGlobal.currentLanguage) {
                ProjectConst.LANGUAGE_CN -> {
                    iv_change_language.setImageResource(R.mipmap.ic_language_en)
                    if (placeNameEN.isEmpty()) {
                        tv_title.text = ProjectConst.EN_TITLE
                    } else {
                        tv_title.text = ProjectConst.EN_TITLE + " —— " + placeNameEN
                    }
                    SpUtil.open(this).putSpData(ProjectConst.SP_LANGUAGE, ProjectConst.LANGUAGE_EN)
                    ProjectGlobal.currentLanguage = ProjectConst.LANGUAGE_EN
                }
                ProjectConst.LANGUAGE_EN -> {
                    iv_change_language.setImageResource(R.mipmap.ic_language_cn)
                    if (placeName.isEmpty()) {
                        tv_title.text = ProjectConst.CN_TITLE
                    } else {
                        tv_title.text = ProjectConst.CN_TITLE + " —— " + placeName
                    }
                    SpUtil.open(this).putSpData(ProjectConst.SP_LANGUAGE, ProjectConst.LANGUAGE_CN)
                    ProjectGlobal.currentLanguage = ProjectConst.LANGUAGE_CN
                }
            }
            EventBus.getDefault().post(LanguageEvent(ProjectGlobal.currentLanguage))
        }

        img_icon.setOnClickListener {
            if (exitCount == 0) {
                baseHandler?.sendEmptyMessageDelayed(msgExitCountReset, 3000)
            }
            exitCount++
            if (exitCount >= 3) {
                exitCount = 0
                baseHandler?.removeMessages(msgExitCountReset)
                finish()
            }
        }

        // 拦截扫码器回调,获取扫码内容
        scanKeyManager = ScanKeyManager(ScanKeyManager.OnScanValueListener { value ->
            getQRCode = value
            if (venueId == -1) {
                toastFail(resources.getString(R.string.place_title))
            } else {
                when (ProjectGlobal.currentLanguage) {
                    ProjectConst.LANGUAGE_CN -> showLoading(ProjectConst.CN_LOADING)
                    ProjectConst.LANGUAGE_EN -> showLoading(ProjectConst.EN_LOADING)
                }
                mPresenter?.getSignInfo(getQRCode, venueId)
            }
        })

        tv_title.setOnClickListener {
            test()
        }
    }

    private fun initBroadcast() {
        statusReceiver = object : BroadcastReceiver() {
            @SuppressLint("SetTextI18n")
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    BroadcastConst.ACTION_STATUS -> {
                        if (intent.hasExtra(BroadcastConst.KEY_STATUS)) {
                            showFragmentByStatus(intent.getStringExtra(BroadcastConst.KEY_STATUS))
                        }
                        if (intent.hasExtra(BroadcastConst.KEY_PLACE_NAME)) {
                            placeName = intent.getStringExtra(BroadcastConst.KEY_PLACE_NAME)
                            initTitle()
                        }
                        if (intent.hasExtra(BroadcastConst.KEY_PLACE_NAME)) {
                            placeNameEN = intent.getStringExtra(BroadcastConst.KEY_PLACE_NAME_EN)
                            initTitle()
                        }
                        if (intent.hasExtra(BroadcastConst.KEY_PLACE_ID)) {
                            venueId = intent.getIntExtra(BroadcastConst.KEY_PLACE_ID, -1)
                        }

                        if (intent.hasExtra(BroadcastConst.KEY_HOME_LIST_POSITION)) {
                            homeListPosition = intent.getIntExtra(BroadcastConst.KEY_HOME_LIST_POSITION, 0)
                        }

                        if (intent.hasExtra(BroadcastConst.KEY_HOME_VIDEO_PROGRESS)) {
                            homeVideoProgress = intent.getIntExtra(BroadcastConst.KEY_HOME_VIDEO_PROGRESS, 0)
                        }
                        if (intent.hasExtra(BroadcastConst.KEY_IS_USE_CUSTOM_ERROR_MSG)) {
                            isUseCustomErrorMsg = intent.getBooleanExtra(BroadcastConst.KEY_IS_USE_CUSTOM_ERROR_MSG, false)
                        }
                    }
                }
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(statusReceiver, IntentFilter(BroadcastConst.ACTION_STATUS))
    }

    private fun showFragmentByStatus(status: String) {
        when (status) {
            BroadcastConst.VALUE_PLACE -> {
                this.addFragment(placeFragment, R.id.fl_container)
                clock_view.visibility = View.INVISIBLE
                iv_change_language.visibility = View.INVISIBLE
            }
            BroadcastConst.VALUE_HOME -> {
                homeFragment = HomeFragment.newInstance(homeListPosition, homeVideoProgress)
                this.replaceFragment(homeFragment, R.id.fl_container)
                clock_view.visibility = View.VISIBLE
                iv_change_language.visibility = View.VISIBLE
            }
            BroadcastConst.VALUE_SIGN -> {
                signInFragment = SignInFragment.newInstance(signInModel!!, getQRCode, venueId)
                this.replaceFragment(signInFragment, R.id.fl_container)
                clock_view.visibility = View.VISIBLE
                iv_change_language.visibility = View.VISIBLE
            }
            BroadcastConst.VALUE_ERROR -> {
                errorFragment = if (isUseCustomErrorMsg) {
                    ErrorFragment.newInstance(errorMsgCN, errorMsgEN)
                } else {
                    ErrorFragment.newInstance()
                }
                this.replaceFragment(errorFragment, R.id.fl_container)
                clock_view.visibility = View.VISIBLE
                iv_change_language.visibility = View.VISIBLE
            }
            BroadcastConst.VALUE_HOME_UPDATE_TASK -> {
                if (homeFragment.isAdded) {
                    homeFragment = HomeFragment.newInstance(homeListPosition, homeVideoProgress)
                    this.replaceFragment(homeFragment, R.id.fl_container)
                    clock_view.visibility = View.VISIBLE
                    iv_change_language.visibility = View.VISIBLE
                }
            }
        }
    }

    /*监听键盘事件,除了返回事件都将它拦截,使用我们自定义的拦截器处理该事件*/
    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        if (event?.keyCode != KeyEvent.KEYCODE_BACK && event?.keyCode != KeyEvent.KEYCODE_VOLUME_DOWN && event?.keyCode != KeyEvent.KEYCODE_VOLUME_UP && event?.keyCode != KeyEvent.KEYCODE_VOLUME_MUTE) {
            scanKeyManager?.analysisKeyEvent(event)
            return true
        }
        return super.dispatchKeyEvent(event)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::statusReceiver.isInitialized) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(statusReceiver)
        }
        if (baseHandler != null) {
            baseHandler?.removeMessages(msgExitCountReset)
            baseHandler?.sendEmptyMessage(baseHandler!!.msgActivityGone)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun signInfoSuccess(data: SignInModel) {
        this.signInModel = data
        if (data.language == "zh-CN") {
            SpUtil.open(this).putSpData(ProjectConst.SP_LANGUAGE, ProjectConst.LANGUAGE_CN)
            ProjectGlobal.currentLanguage = ProjectConst.LANGUAGE_CN
            iv_change_language.setImageResource(R.mipmap.ic_language_cn)
            if (placeName.isEmpty()) {
                tv_title.text = ProjectConst.CN_TITLE
            } else {
                tv_title.text = ProjectConst.CN_TITLE + " —— " + placeName
            }
        } else {
            SpUtil.open(this).putSpData(ProjectConst.SP_LANGUAGE, ProjectConst.LANGUAGE_EN)
            ProjectGlobal.currentLanguage = ProjectConst.LANGUAGE_EN
            iv_change_language.setImageResource(R.mipmap.ic_language_en)
            if (placeNameEN.isEmpty()) {
                tv_title.text = ProjectConst.EN_TITLE
            } else {
                tv_title.text = ProjectConst.EN_TITLE + " —— " + placeNameEN
            }
        }
        if (data.find_status == 1) {
            showFragmentByStatus(BroadcastConst.VALUE_SIGN)
        } else {
            errorMsgCN = data.un_find_msg
            errorMsgEN = data.un_find_msg_en
            isUseCustomErrorMsg = true
            showFragmentByStatus(BroadcastConst.VALUE_ERROR)
        }
    }

    override fun signInfoFailed() {
        isUseCustomErrorMsg = false
        showFragmentByStatus(BroadcastConst.VALUE_ERROR)
    }

    //todo
    private fun test() {
        getQRCode = "MTItLS0xNg=="
        if (venueId == -1) {
            toastFail(resources.getString(R.string.place_title))
        } else {
            when (ProjectGlobal.currentLanguage) {
                ProjectConst.LANGUAGE_CN -> showLoading(ProjectConst.CN_LOADING)
                ProjectConst.LANGUAGE_EN -> showLoading(ProjectConst.EN_LOADING)
            }
            mPresenter?.getSignInfo(getQRCode, venueId)
        }
    }

    private fun setTimerTask() {
        val intent = Intent(this, AlarmReceiver::class.java)
        val sender = PendingIntent.getBroadcast(this, 0, intent, 0)
        val calendar = Calendar.getInstance()
        // 这里时区需要设置一下，不然会有8个小时的时间差
        calendar.timeZone = TimeZone.getTimeZone("GMT+8")
        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 30)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        var selectTime = calendar.timeInMillis
        // 如果当前时间大于设置的时间，那么就从第二天的设定时间开始
        val systemTime = System.currentTimeMillis()
        if (systemTime > selectTime) {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
            selectTime = calendar.timeInMillis
        }
        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        Log.d("Timer", "selectTime>>$selectTime")
        manager.setRepeating(AlarmManager.RTC, selectTime, 24 * 60 * 60 * 1000, sender)
    }
}