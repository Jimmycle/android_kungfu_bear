package com.jimmy.kungfu_bear.network.log

import android.os.Handler
import android.os.Looper
import com.google.gson.Gson
import com.jimmy.kungfu_bear.util.HttpLUtil
import com.jimmy.kungfu_bear.util.JsonUtil
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.Response
import okio.Buffer
import java.io.EOFException
import java.net.URLDecoder
import java.nio.charset.Charset

/**
 * 日志拦截器
 */
object LogInterceptor {

    fun init() = Interceptor { chain ->
        val request = chain.request()
        val url = request.url().toString()
        val method = request.method()
        val requestHeader = Gson().toJson(request.headers().toMultimap())
        val reqBody = request.body()
        var requestBody: String? = ""
        var requestBodySize: Long = 0
        if (null != reqBody) {
            if (reqBody is MultipartBody) {
                requestBody = Gson().toJson(reqBody.part(0).headers()?.toMultimap())
            } else {
                val buffer = Buffer()
                reqBody.writeTo(buffer)
                try {
                    requestBody = if (isPlaintext(
                            buffer
                        )
                    ) {
                        URLDecoder.decode(buffer.readString(Charset.forName("UTF-8")), "UTF-8")
                    } else {
                        buffer.readString(Charset.forName("UTF-8"))
                    }
                    requestBodySize = requestBody?.length!!.toLong()
                } catch (e: Exception) {
                    e.printStackTrace()
                    requestBody = e.message
                }
            }
        }
        val startTime = System.currentTimeMillis()
        val response: Response?
        try {
            response = chain.proceed(request)
        } catch (e: Exception) {
            throw e
        }
        if (response != null) {
            val useTime = System.currentTimeMillis() - startTime
            val responseCode = response.code()
            val responseHeader = Gson().toJson(response.headers().toMultimap())
            val respBody = response.body()
            var responseBody: String? = ""
            var responseBodySize: Long = 0
            try {
                val source = respBody?.source()
                source?.request(java.lang.Long.MAX_VALUE)
                val buffer = source?.buffer()
                if (respBody?.contentLength() != 0L) {
                    responseBody = buffer?.clone()?.readString(Charset.forName("UTF-8"))
                    responseBodySize = responseBody?.length!!.toLong()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                responseBody = e.message
            }
            Handler(Looper.getMainLooper()).post {
                val logEntity = LogEntity()
                logEntity.id = startTime
                logEntity.requestUrl = url
                logEntity.startTimestamp = startTime
                logEntity.useTime = useTime
                logEntity.requestMethod = method
                logEntity.requestHeader = requestHeader
                logEntity.responseCode = responseCode
                logEntity.responseHeader = responseHeader
                logEntity.requestBody = requestBody
                logEntity.requestBodySize = requestBodySize
                logEntity.responseBody = responseBody
                logEntity.responseBodySize = responseBodySize
                saveLog(logEntity)
                showLog(logEntity, false)
            }
        }
        response.newBuilder().build()
    }

    /**
     * Returns true if the body in question probably contains human readable text. Uses a small sample
     * of code points to detect unicode control characters commonly used in binary file signatures.
     */
    private fun isPlaintext(buffer: Buffer): Boolean {
        try {
            val prefix = Buffer()
            val byteCount = if (buffer.size() < 64) buffer.size() else 64
            buffer.copyTo(prefix, 0, byteCount)
            for (i in 0..15) {
                if (prefix.exhausted()) {
                    break
                }
                val codePoint = prefix.readUtf8CodePoint()
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false
                }
            }
            return true
        } catch (e: EOFException) {
            // Truncated UTF-8 sequence.
            return false
        }
    }

    /**
     * 保存本地数据库
     */
    private fun saveLog(logEntity: LogEntity) {
        LogRealmManager.insert(logEntity)
    }

    /**
     * 显示Log
     */
    private fun showLog(logEntity: LogEntity, isShowHeader: Boolean) {
        val buffer = StringBuffer()
        buffer.append("\n-----------------------------NetWork-----------------------------")
        buffer.append("\n-----------------------------Request-----------------------------")
        buffer.append("\n--> url：" + logEntity.requestMethod + ' ' + logEntity.requestUrl + ' ' + logEntity.requestProtocol)
        if (isShowHeader) {
            buffer.append("\n--> header：" + JsonUtil.jsonFormat(logEntity.requestHeader!!))
        }
        if (!logEntity.requestBody.isNullOrEmpty()) {
            buffer.append("\n--> body：" + JsonUtil.jsonFormat(logEntity.requestBody!!) + " size：(${logEntity.requestBodySize} B)")
        }
        buffer.append("\n-----------------------------Response-----------------------------")
        buffer.append("\n<-- code：" + logEntity.responseCode)
        if (isShowHeader) {
            buffer.append("\n<-- header：" + JsonUtil.jsonFormat(logEntity.responseHeader!!))
        }
        buffer.append("\n<-- body：" + JsonUtil.jsonFormat(logEntity.responseBody!!) + " size：(${logEntity.responseBodySize} B)")
        buffer.append("\n<-- end use time：" + logEntity.useTime)
        HttpLUtil.i(buffer.toString())
    }
}



