package com.jimmy.kungfu_bear.model

/**
"data": {
"language": "zh-CN",
"find_status": 1,
"un_find_msg": "",
"un_find_msg_en": "",
"name": "翟宇鑫",
"data": [
{
"id": 2,
"child_id": 16,
"sign_status": "1",
"name": "小猴",
"lesson_type": 1,
"pro_title": "功夫课",
"pro_title_en": "2_test",
"start_time": "12:00",
"end_time": "13:00",
"master_name": "1",
"venue_title": "小武馆",
"venue_id": 1,
"created_at": null,
"start_status": 3,
"start_status_title": "已结束",
"start_status_title_en": "already over",
"status_msg": "未签到",
"status_msg_en": "2",
"date": "2020-01-20"
},
{
"id": 3,
"child_id": 16,
"sign_status": "1",
"name": "小猴",
"lesson_type": 1,
"pro_title": "功夫课",
"pro_title_en": "2_test",
"start_time": "19:22",
"end_time": "20:22",
"master_name": "1",
"venue_title": "小武馆",
"venue_id": 1,
"created_at": "2020-01-20 10:58:58",
"start_status": 1,
"start_status_title": "未开始",
"start_status_title_en": "Not yet begun",
"status_msg": "未签到",
"status_msg_en": "2",
"date": "2020-01-20"
}
],
"child_name": "小猴"
"member_id": 1000012,
}
 */

data class SignInModel(
    val language: String, //zh-CN 中文
    val find_status: Int,  //0 异常状态,显示un_find_msg或者un_find_msg_en 1 正常状态显示
    val un_find_msg: String,
    val un_find_msg_en: String,
    val name: String,
    val child_name: String,
    val member_id: Long,
    val data: List<SignInItem>
)

data class SignInItem(
    val id: Int,
    val child_id: Int,
    var sign_status: String, // sign_status 1.未签到 2.已签到
    val name: String,
    val lesson_type: String,
    val pro_title: String,
    val pro_title_en: String,
    val start_time: String,
    val end_time: String,
    val master_name: String,
    val venue_title: String,
    val venue_id: Int,
    val created_at: String,
    val start_status: String, //start_status 1.未开始 2/已开始 3.已结束
    val start_status_title: String,
    val start_status_title_en: String,
    val date: String,
    val status_msg: String,
    val status_msg_en: String
)


