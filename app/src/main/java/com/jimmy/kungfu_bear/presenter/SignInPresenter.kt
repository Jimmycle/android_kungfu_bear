package com.jimmy.kungfu_bear.presenter

import com.jimmy.kungfu_bear.base.BasePresenter
import com.jimmy.kungfu_bear.contract.SignInContract
import com.jimmy.kungfu_bear.network.BearApi
import com.trello.rxlifecycle3.components.support.RxFragment
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class SignInPresenter(fragment: RxFragment, view: SignInContract.View) : BasePresenter<SignInContract.View>(fragment, view), SignInContract.Presenter {

    override fun sign(code: String, id: List<Int>) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("code", code)
            jsonObject.put("id", JSONArray(id))
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())
        BearApi.getService(mFragment?.activity!!).sign(requestBody).execute {
            onNext {
                if (it.isOk) {
                    mView?.signSuccess()
                } else {
                    mView?.signFailed()
                }
            }
            onError {
                mView?.signFailed()
            }
        }
    }

    override fun getSignInfo(code: String, venue_id: Int) {
        BearApi.getService(mFragment?.activity!!).getSignInfo(code, venue_id).execute {
            onNext {
                if (it.hasData) {
                    mView?.signInfoSuccess(it.data!!)
                } else {
                    mView?.signInfoFailed()
                }
            }
            onError {
                mView?.signInfoFailed()
            }
        }
    }

}