package com.jimmy.kungfu_bear.network

class BearResponse<T> {
    /**
     * 200 成功
     */
    var code: Int = 0
    var msg: String? = null
    var data: T? = null
    var exception: Throwable? = null

    val isOk: Boolean
        get() = (code == 200)

    val hasData: Boolean
        get() = isOk && data != null

}