package com.jimmy.kungfu_bear.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.jimmy.kungfu_bear.util.Ex_T0_Unit
import com.jimmy.kungfu_bear.util.SubscriberHelper
import com.trello.rxlifecycle3.LifecycleTransformer
import com.trello.rxlifecycle3.components.support.RxAppCompatActivity
import com.trello.rxlifecycle3.components.support.RxFragment
import com.trello.rxlifecycle3.components.support.RxFragmentActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class BasePresenter<T : BaseContract.IView> : BaseContract.IPresenter<T> {
    protected var mActivity: RxAppCompatActivity? = null
    protected var mFragmentActivity: RxFragmentActivity? = null
    protected var mFragment: RxFragment? = null
    protected var mView: T? = null
    private var mLifecycleOwner: LifecycleOwner? = null
    lateinit var bindToLifecycle: Any

    constructor(activity: RxAppCompatActivity, view: T) {
        this.mActivity = activity
        this.mView = view
    }

    constructor(activity: RxFragmentActivity, view: T) {
        this.mFragmentActivity = activity
        this.mView = view
    }

    constructor(fragment: RxFragment, view: T) {
        this.mFragment = fragment
        this.mView = view
    }

    override fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        this.mLifecycleOwner = lifecycleOwner
    }

    override fun onCreate(owner: LifecycleOwner) {
    }

    override fun onDestroy(owner: LifecycleOwner) {
        if (mView != null) {
            mView = null
        }
    }

    override fun onLifecycleChanged(owner: LifecycleOwner, event: Lifecycle.Event) {
    }

    fun <T> Observable<T>.execute(init: Ex_T0_Unit<SubscriberHelper<T>>) {
        val subscriberHelper = SubscriberHelper<T>()
        init(subscriberHelper)
        lateinit var bindToLifecycle: LifecycleTransformer<T>
        when {
            mActivity != null -> bindToLifecycle = mActivity!!.bindToLifecycle<T>()
            mFragmentActivity != null -> bindToLifecycle = mFragmentActivity!!.bindToLifecycle<T>()
            mFragment != null -> bindToLifecycle = mFragment!!.bindToLifecycle<T>()
        }
        subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindToLifecycle)
                .subscribe(subscriberHelper)
    }
}